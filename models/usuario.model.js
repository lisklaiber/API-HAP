'use strict'

class Usuario{
	constructor(correo, pass, nombre, apPaterno, apMaterno, foto, color, idEmpresa){
		this.Correo = correo;
		this.Pass = pass;
		this.Nombre = nombre;
		this.Ap_Paterno = apPaterno;
		this.Ap_Materno = apMaterno;
	    this.Foto = foto;
	    this.Color = color;
	    this.ID_Empresa = idEmpresa;
	}
}

module.exports = Usuario;