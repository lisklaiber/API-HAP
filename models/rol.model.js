'use strict'

class Rol{
	constructor(descripcion, nombre, color, icono, idEmpresa){
		this.Descripcion = descripcion;
		this.Nombre = nombre;
		if(color == '')
			this.color = '#000';
		else
    		this.Color = color;
    	this.Icono = icono;
    	this.ID_Empresa = idEmpresa
	}
}

module.exports = Rol;