'use strict'
class Ponderacion{
	constructor(completado, tiempo, costo, idProceso){
		this.Completado = completado;
    	this.Tiempo = tiempo;
    	this.Costo = costo;
    	this.ID_Proceso = idProceso;
	}
}

module.exports = Ponderacion;