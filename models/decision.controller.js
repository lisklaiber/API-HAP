'use strict'

class Decision {
	constructor(tipo, nombre, descripcion, condicion){
		this.Tipo = tipo;
    	this.Nombre = nombre
    	this.Descripcion = descripcion;
    	this.Condicion = condicion;
	}
}

module.exports = Decision;