class Serial {
	constructor(idProceso, idSubProceso, idSeriado, tipoSubProceso, tipoSeriado){
		this.ID_Proceso = idProceso;
    	this.ID_Sub_Proceso = idSubProceso;
    	this.ID_Seriado = idSeriado;
    	this.Tipo_Sub_Proceso = tipoSubProceso;
    	this.Tipo_Seriado = tipoSeriado;
	}

}

module.exports = Serial;