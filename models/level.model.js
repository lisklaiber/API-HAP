class Level{
	constructor(idProceso, padre, hijo, tipoHijo){
		this.ID_Proceso = idProceso;
    	this.Padre = padre;
    	this.Hijo = hijo;
    	this.Tipo_Hijo = tipoHijo;
	}
}

module.exports = Level;