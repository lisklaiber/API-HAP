'use strict'

class Proceso {
	
	constructor(subID, nombre, abreviatura, fechaIni, fechaFin, duracion, obligatorio, tipo, completado, 
		descripcion, costo, idEmpresa, idProcBase){

		this.Sub_ID = subID;
		this.Nombre = nombre;
		this.Abreviatura = abreviatura;
		this.Fecha_Ini = fechaIni;
		this.Fecha_Fin = fechaFin;
		this.Duracion = duracion;
		this.Obligatorio = obligatorio;
		this.Tipo = tipo;
		this.Completado = completado;
		this.Descripcion = descripcion;
		this.Costo = costo;
		this.ID_Empresa = idEmpresa;
		this.ID_Proc_Base = idProcBase;

	}

}

module.exports = Proceso;