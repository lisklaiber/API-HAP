'use strict'

class Insumo{
	constructor(nombre, fechaEntrega, obligatorio, tipo, completado, descripcion, archivo, costo){
		this.Nombre = nombre; 
		this.Fecha_Entrega = fechaEntrega; 
		this.Obligatorio = obligatorio;
		this.Tipo = tipo;
		this.Completado = completado;
		this.Descripcion = descripcion;
		this.Archivo = archivo; 
		this.Costo = costo;
	}
}

module.exports = Insumo;