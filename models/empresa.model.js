'use strict'
class Empresa{
	constructor(nombre, acronimo, direccion, logo, color){
		this.Nombre = nombre;
		this.Acronimo = acronimo;
		this.Direccion = direccion;
		this.Logo = logo;
		this.Color = color;
	}
}

module.exports = Empresa;