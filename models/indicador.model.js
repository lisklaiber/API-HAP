class Indicador{
	constructor(nombre, verdeMax, verdeMin, descripcion, idProceso){
 		this.Nombre = nombre;
		this.VERDE_MAX = verdeMax;
	    this.VERDE_MIN = verdeMin;
		this.Descripcion = descripcion;
		this.ID_Proceso = idProceso;
 	}
}

 class IndicadorComp extends Indicador{
 	constructor(nombre, verdeMax, verdeMin, amMax, amMin, rojo, descripcion, idProceso){
 		super(nombre, verdeMax, verdeMin, descripcion, idProceso);

		this.AM_MAX = amMax;
	    this.AM_MIN = amMin;
		this.ROJO = rojo;
 	}
 }

 class IndicadorTC extends Indicador{
 	constructor(nombre, tipo, verdeMax, verdeMin, amSupMax, amSupMin, amInfMax, amInfMin, rojoSup, 
 		rojoInf, descripcion, idProceso){
 		
 		super(nombre, verdeMax, verdeMin, descripcion, idProceso);

 		this.Tipo = tipo;
 		this.AM_SUP_MAX = amSupMax;
	    this.AM_SUP_MIN = amSupMin;
		this.AM_INF_MAX = amInfMax;
		this.AM_INF_MIN = amInfMin;
		this.ROJO_SUP = rojoSup;
		this.ROJO_INF = rojoInf;
 	}
 }

 module.exports = {
 	Indicador,
 	IndicadorComp,
 	IndicadorTC
 }