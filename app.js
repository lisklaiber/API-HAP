'use strict'
let express = require('express');
let bodyParser = require("body-parser");

let app = express();
let apiRol = require("./routes/rol.route.js");
let apiEmpresa = require('./routes/empresa.route.js');
let apiProceso = require('./routes/proceso.route.js');
let apiUsuario = require('./routes/usuario.route.js');
let apiAcceso = require('./routes/acceso.route.js');
let apiPlantilla = require('./routes/plantilla.route.js');
let apiEvento = require('./routes/evento.route.js');
let apiDecision = require('./routes/decision.route.js');
let apiSerial = require('./routes/serial.route.js');
let apiLevel = require('./routes/level.route.js');
let apiInsumo = require('./routes/insumo.route.js');
let apiIndicador = require('./routes/indicador.route.js');
let apiEstrategia = require('./routes/estrategia.route.js');
let apiPonderacion = require('./routes/ponderacion.route.js');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Acces-Control-Request-Method, enctype');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTION, PUT, DELETE');

	next();
});

app.use('/api/empresa', apiEmpresa);
app.use('/api/proceso', apiProceso);
app.use('/api/usuario', apiUsuario);
app.use('/api/acceso', apiAcceso);
app.use('/api/plantilla', apiPlantilla);
app.use('/api/evento', apiEvento);
app.use('/api/decision', apiDecision);
app.use("/api/rol", apiRol);
app.use('/api/serial', apiSerial);
app.use('/api/level', apiLevel);
app.use('/api/insumo', apiInsumo);
app.use('/api/indicador', apiIndicador);
app.use('/api/estrategia', apiEstrategia);
app.use('/api/ponderacion', apiPonderacion);

module.exports = app;