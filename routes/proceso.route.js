'use strict'

let express = require('express');
let procesoController = require('../controllers/proceso.controller.js');
let api = express.Router();

api.get('/ValidarProceso/:nombre/:idEmpresa', procesoController.ValidarProceso);
api.post('/AddProceso', procesoController.AddProceso);
api.put('/UpdateProceso/:idProceso', procesoController.UpdateProceso);
api.delete('/DeleteProceso/:idProceso', procesoController.DeleteProceso);
api.get('/GetDetailProceso/:idProceso', procesoController.GetDetailProceso);
api.post('/createProceso', procesoController.CreateProceso);
api.get('/GetProcesosBase/:idEmpresa', procesoController.GetProcesosBase);
api.post('/insertIndicadores', procesoController.InsertIndicadores);
api.get('/getProceso/:idProceso', procesoController.getProceso);
api.get('/getProcPrin/:idProceso', procesoController.GetProcesoPrincipal);
api.get('/getPendientes/:idProceso', procesoController.GetPendientes);
api.get('/UpdateStatus/:idProceso/:status', procesoController.UpdateStatus);
api.get('/GetProcesos/:idProcBase', procesoController.GetProcesos);
api.get('/GetResponsable/:idProceso', procesoController.GetResponsable);
api.get('/AddCreacionProceso/:idProceso', procesoController.AddCreacionProceso);
api.get('/UpdateCreacionProceso/:idProceso', procesoController.UpdateCreacionProceso);
api.post('/AddRoles', procesoController.AddRoles);
api.delete('/DeleteRol/idAsignacion', procesoController.DeleteRol);
api.put('/UpdateRol/:idAsignacion', procesoController.UpdateRol);
api.get('/GetIdsProcesos/:idProcBase', procesoController.GetIdsProcesos);

module.exports = api;