'use strict'

let express = require('express');
let ponderacionController = require('../controllers/ponderacion.controller.js');

let api = express.Router();

api.post('/AddPonderacion', ponderacionController.AddPonderacion);
api.put('/UpdatePonderacion/:idPonderacion', ponderacionController.UpdatePonderacion);
api.get('/GetPonderacion/:idPonderacion', ponderacionController.GetPonderacion);
api.delete('/DeletePonderacion/:idPonderacion', ponderacionController.DeletePonderacion);

module.exports = api;