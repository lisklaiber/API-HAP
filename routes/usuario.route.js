'use strict'

let express = require('express');
let multer  = require('multer');
let usuarioController = require('../controllers/usuario.controller.js');
let api = express.Router();
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './Imgs/Usuario/');
  },
  filename: (req, file, cb) => {
    cb(null, `${req.body.correo}-${file.originalname}`);
  }
});
let storageUpdate = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './Imgs/Usuario/');
  },
  filename: (req, file, cb) => {
  	if (req.body.nombreFoto == '') {
  		cb(null, `${req.body.correo}-${file.originalname}`);
  	}
  	else
    	cb(null, `${req.body.nombreFoto}`);
  }
});

let upload = multer({ storage: storage });
let changeFoto = multer({ storage: storageUpdate });

api.post('/ValidarCorreo', usuarioController.ValidarCorreo);
api.post('/AddUsuario', upload.single('foto'), usuarioController.AddUsuario);
api.put('/UpdateUsuario/:idUsuario', usuarioController.UpdateUsuario);
api.put('/ChangePass/:idUsuario', usuarioController.ChangePass);
api.put('/ChangeFoto/:idUsuario', changeFoto.single('foto'), usuarioController.ChangeFoto)
api.get('/GetUsuarios/:idEmpresa', usuarioController.GetUsuarios);
api.get('/GetRoles/:idUsuario', usuarioController.GetRoles);
api.get('/GetUsuario/:idUsuario', usuarioController.GetUsuario)
api.delete('/DeleteUsuario/:idUsuario', usuarioController.DeleteUsuario);
api.get('/GetFoto/:foto', usuarioController.GetFoto);
api.post('/AddRol', usuarioController.AddRol);
api.post('/AsignarProceso', usuarioController.AsignarProceso);
api.get('/GetProcesos/:idUsuario', usuarioController.GetProcesos);

module.exports = api;