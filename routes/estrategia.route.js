'use strict'

let express = require('express');
let estrategiaController = require('../controllers/estrategia.controller.js');

let api = express.Router();

api.post('/AddEstrategia/:tipo', estrategiaController.AddEstrategia);
api.put('/UpdateEstrategia/:tipo/:idEstrategia', estrategiaController.UpdateEstrategia);
api.get('/GetEstrategia/:tipo/:idEstrategia', estrategiaController.GetEstrategia);
api.get('/GetEstrategias/:tipo/:idIndicador', estrategiaController.GetEstrategias);
api.delete('/DeleteEstrategia/:tipo/:idEstrategia', estrategiaController.DeleteEstrategia);
api.delete('/DeleteEstrategias/:tipo/:idIndicador', estrategiaController.DeleteEstrategias);

module.exports = api;