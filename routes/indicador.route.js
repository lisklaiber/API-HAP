'use strict'

let express = require('express');
let indicadorController = require('../controllers/indicador.controller.js');

let api = express.Router();

api.post('/AddIndicador/:tipo', indicadorController.AddIndicador);
api.put('/UpdateIndicador/:tipo/:idIndicador', indicadorController.UpdateIndicador);
api.get('/GetIndicador/:tipo/:idIndicador', indicadorController.GetIndicador);
api.get('/GetIndicadores/:idProceso', indicadorController.GetIndicadores);
api.delete('/DeleteIndicador/:tipo/:idIndicador', indicadorController.DeleteIndicador);
api.delete('/DeleteIndicadores/:idProceso', indicadorController.DeleteIndicadores);

module.exports = api;