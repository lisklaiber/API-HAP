'use strict'

let express = require('express');
let levelController = require('../controllers/level.controller.js');
let api = express.Router();

api.post('/AddLevel', levelController.AddLevel);
api.put('/UpdateLevel/:idLevel', levelController.UpdateLevel);
api.get('/GetLevel/:idLevel', levelController.GetLevel);
api.get('/GetHijos/:idProceso', levelController.GetHijos);
api.get('/GetLevels/:idProceso', levelController.GetLevels);
api.delete('/DeleteLevel/:idLevel', levelController.DeleteLevel);
api.delete('/DeleteLevels/:idProceso', levelController.DeleteLevels);

module.exports = api;