'use strict'

let express = require('express');

let rolController = require('../controllers/rol.controller.js');
let api = express.Router();

api.get('/ValidarRol/:nombre/:idEmpresa', rolController.ValidarRol);
api.get('/GetRoles/:idEmpresa', rolController.GetRoles);
api.post('/addRol', rolController.AddRol);
api.put('/UpdateRol/:idRol', rolController.UpdateRol);
api.delete('/DeleteRol/:idRol', rolController.DeleteRol);
api.get('/GetRol/:idRol', rolController.GetRol);
api.put('/UpdatePermisos/:idRelacion', rolController.UpdatePermisos);
api.get('/GetPermisos/:idRol', rolController.GetPermisos);
api.get('/GetUsuarios/:idRol', rolController.GetUsuarios);

module.exports = api;