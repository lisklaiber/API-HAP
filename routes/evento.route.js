'use strict'

let express = require('express');
let eventoController = require('../controllers/evento.controller.js');

let api = express.Router();

api.post('/AddEvento', eventoController.AddEvento);
api.put('/UpdateEvento/:idEvento', eventoController.UpdateEvento);
api.get('/GetEvento/:idEvento', eventoController.GetEvento);
api.delete('/DeleteEvento/:idEvento', eventoController.DeleteEvento);
api.post('/GetEventos', eventoController.GetEventos);

module.exports = api;