'use strict'

let express = require('express');
let serialController = require('../controllers/serial.controller.js');

let api = express.Router();

api.post('/AddSerial', serialController.AddSerial);
api.put('/UpdateSerial/:idSerial', serialController.UpdateSerial);
api.get('/GetSerial/:idSerial', serialController.GetSerial);
api.get('/GetSerialsProceso/:idProceso', serialController.GetSerialsProceso);
api.get('/GetSerials/:idProceso/:tipo',serialController.GetSerials);
api.delete('/DeleteSerial/:idSerial', serialController.DeleteSerial);
api.delete('/DeleteSerials/:idProceso', serialController.DeleteSerials);

module.exports = api;