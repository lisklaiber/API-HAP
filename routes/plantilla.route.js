'use strict'

let express = require('express');
let plantillaController = require('../controllers/plantilla.contoller.js');

let api = express.Router();

api.get('/ValidarPlantilla/:nombre/:idEmpresa', plantillaController.ValidarPlantilla);
api.post('/AddPlantilla', plantillaController.AddPlantilla);
api.post('/AddProcToPlant', plantillaController.AddProcToPlant);
api.get('/GetPlantillas/:idEmpresa', plantillaController.GetPlantillas);
api.get('/GetProcPlant/:idPlantilla', plantillaController.GetProcPlant);
api.get('/GetPlantilla/:idPlantilla', plantillaController.GetPlantilla);
api.delete('/DeletePlantilla/:idPlantilla', plantillaController.DeletePlantilla);
api.post('/AddProceso', plantillaController.AddProceso);
api.delete('/DeleteProceso/idRelacion', plantillaController.DeleteProceso);
api.put('/UpdatePlantilla/:idPlantilla', plantillaController.UpdatePlantilla);

module.exports = api;