'use strict'

let express = require('express');
let decisionController = require('../controllers/decision.controller.js');

let api = express.Router();

api.post('/AddDecision', decisionController.AddDecision);
api.put('/UpdateDecision/:idDecision', decisionController.UpdateDecision);
api.get('/GetDecision/:idDecision', decisionController.GetDecision);
api.delete('/DeleteDecision/:idDecision', decisionController.DeleteDecision);
api.post('/GetDecisiones', decisionController.GetDecisiones);

module.exports = api;