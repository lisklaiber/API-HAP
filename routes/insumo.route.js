'use strict'

let express = require('express');
let multer  = require('multer');
let insumoController = require('../controllers/insumo.controller.js');
let api = express.Router();
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './Uploads/');
  },
  filename: (req, file, cb) => {
  	// console.log('files',file)
    cb(null, `${req.params.idInsumo}-${Date.now()}-${file.originalname}`);
  }
});
let upload = multer({ storage: storage });
api.post('/AddInsumo', insumoController.AddInsumo);
api.post('/AddRelInsumo', insumoController.AddRelInsumo);
api.put('/UpdateInsumo', insumoController.UpdateInsumo);
api.get('/GetInsumo/:idInsumo', insumoController.GetInsumo);
api.get('/GetInsumos/:idProceso/:tipo?', insumoController.GetInsumos);
api.delete('/DeleteInsumo/:idInsumo', insumoController.DeleteInsumo);
api.post('/DeleteInsumos', insumoController.DeleteInsumos);
api.delete('/DeleteRelInsumos/:idObjeto/:tipo?', insumoController.DeleteRelInsumos);
api.put('/UploadFile/:idInsumo', upload.array('archivos'),insumoController.UploadFile);
api.get('/GetFile/file', insumoController.GetFile);

module.exports = api;