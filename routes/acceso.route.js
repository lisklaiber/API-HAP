'use strict'

let express = require('express');
let accesoController = require('../controllers/acceso.controller.js')

let api = express.Router();

api.post('/Autentication', accesoController.Autentication);
api.post('/Acceso', accesoController.Acceso);

module.exports = api;