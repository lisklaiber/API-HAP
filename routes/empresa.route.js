'use strict'

let express = require('express');
let multer  = require('multer');
let empresaController = require('../controllers/empresa.controller.js');
let api = express.Router();
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './Imgs/Empresa/');
  },
  filename: (req, file, cb) => {
    cb(null, `${req.body.acronimo}-${file.originalname}`);
  }
})

let upload = multer({ storage: storage });

api.post('/AddEmpresa', upload.single('logo'), empresaController.AddEmpresa);
api.get('/GetLogo/:logo', empresaController.GetLogo);
api.get('/GetEmpresa/:idEmpresa', empresaController.GetEmpresa);
api.get('/GetEmpresas', empresaController.GetEmpresas);
api.put('/UpdateEmpresa/:idEmpresa', empresaController.UpdateEmpresa);
api.delete('/DeleteEmpresa/:idEmpresa', empresaController.DeleteEmpresa);

module.exports = api;