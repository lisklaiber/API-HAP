'use strict'
const conn = require('./connection.js');
// let AdminPlantilla = require('../controllers/adminPlantilla.js');
let Proceso = require('../models/proceso.model.js');

/*=========================================================
=            Verificamos el nombre del Proceso            =
=========================================================*/

function ValidarProceso(req, res) {
	let nombre = req.params.nombre;
	let idEmpresa = req.params.idEmpresa;
	conn('PROCESO').where({
		'Nombre': nombre,
		'ID_Empresa': idEmpresa,
		'Sub_ID': 0
	}).select('Nombre').then(result => {
		if(result[0])
			res.send({
				resp: "El nombre seleccionado ya existe", 
				data: result[0]
			});
		else
			res.send({resp: "Aprobado", data: result[0]});
	}).catch(error => {
		res.status(500).send({resp: "Ocurrió un error", Error: `${error}`});
	});
}

/*=====  End of Verificamos el nombre del Proceso  ======*/



/*============================================
=            Agregamos un proceso            =
============================================*/

function AddProceso(req, res) {
	
	let proceso = new Proceso(req.body.subID, req.body.nombre, req.body.abreviatura, req.body.fechaIni, req.body.fechaFin,
		req.body.duracion, req.body.obligatorio, req.body.tipo, req.body.completado, req.body.descripcion,
		req.body.costo, req.body.idEmpresa, req.body.idProcBase);

	// console.log(proceso);

	conn('PROCESO').insert(proceso).then(idProceso => {
		if (!idProceso)
			res.status(500).send({ resp: 'error', error: 'no se insertó el proceso' });
		else
			res.status(200).send({ resp: idProceso });
	}).catch(error => {
		res.status(404).send({ resp: 'error', error: `${error}` });
	});

}

/*=====  End of Agregamos un proceso  ======*/



/*======================================
=            Update Proceso            =
======================================*/

function UpdateProceso(req, res) {
	
	let idProceso = req.params.idProceso;
	let proceso = new Proceso(req.body.subID, req.body.nombre, req.body.abreviatura, req.body.fechaIni, req.body.fechaFin,
		req.body.duracion, req.body.obligatorio, req.body.tipo, req.body.completado, req.body.descripcion,
		req.body.costo, req.body.idEmpresa, req.body.idProcBase);

	conn('PROCESO').where('ID_Proceso', idProceso).update(proceso).then(result => {
		
		if( !result )
			res.status(500).send({ resp: 'error', error:'No se actualizó correctamente' });
		else
			res.status(200).send({ resp: 'Proceso actualizado correctamente' });

	}).catch(error => {

		res.status(404).send({ resp: 'error', error: `${error}` });

	});

}

/*=====  End of Update Proceso  ======*/


/*=================================================
=            Eliminación de un proceso            =
=================================================*/

function DeleteProceso(req, res) {
	let idProceso = req.params.idProceso;

	conn('PROCESO').where('ID_Proceso', idProceso).del().then(result =>{
		if( !result )
			res.status(500).send({ resp: 'error', error: 'No se encontró el proceso para eliminarlo' });
		else
			res.status(200).send({ resp: 'Proceso eliminado correctamente' });
	}).catch(error => {
		res.status(404).send( {resp: 'error', error: `${error}`} );
	});
}

/*=====  End of Eliminación de un proceso  ======*/


/*==========================================================
=            Obtenemos el detalle de un proceso            =
==========================================================*/

function GetDetailProceso(req, res) {
	let idProceso = req.params.idProceso;

	conn('PROCESO').where('ID_Proceso', idProceso).then(result => {
		if(!result.length)
			res.status(500).send({ resp: 'error', error: `No se encontró el proceso` });
		else
			res.status(200).send({ resp: result[0] });
	}).catch(error => {
		res.status(404).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos el detalle de un proceso  ======*/



/*================================================================================
=            Creamos el proceso a partir de la plantilla seleccionada            =
================================================================================*/

// funcion para obtener los datos de la plantilla
function getPlantilla(idPlantilla) {
	return conn('PLANTILLA').where('ID_Plantilla', idPlantilla);
}

// funcion para insertar los datos principales del proceso padre
function createProcPadre(data) {
	return conn('PROCESO').insert({
		Sub_ID: 0,
		Nombre: data.Nombre,
		Obligatorio: true,
		Descripcion: data.Descripcion,
		Tipo: 1,
		ID_Empresa: data.ID_Empresa
	});
}

// funcion para obtener los ID de los procesos creados
function getIdsProc(id) {
	return conn('PROCESO').where('ID_Proc_Base', id).select('ID_Proceso', 'Sub_ID').orderBy('Sub_ID', 'asc');
}

function InsertAllDataProcess(procesos, levels, serials, macroProceso, res, insumos) {
	// creamos el proceso padre a partir de los valores de la plantilla
	createProcPadre(macroProceso).then(id => {
		// aggregamos el id del proceso base los procesos
		procesos.map(proceso => {
			proceso.ID_Proc_Base = id[0];
		});
		//console.log(procesos);
		conn('PROCESO').insert(procesos).then(idPimerProc => {
			getIdsProc(id[0]).then(result => {
				// console.log(result);
				for (let i = 0; i < levels.length; i++) {
					levels[i].Padre = result[levels[i].Padre - 1].ID_Proceso;
					levels[i].Hijo = result[levels[i].Hijo - 1].ID_Proceso;
					levels[i].ID_Proceso = id[0];
				}
				for (let i = 0; i < serials.length; i++) {
					serials[i].ID_Sub_Proceso = result[serials[i].ID_Sub_Proceso - 1].ID_Proceso;
					serials[i].ID_Seriado = result[serials[i].ID_Seriado - 1].ID_Proceso;
					serials[i].ID_Proceso = id[0];
				}
				insumos.map(insumo => {
					result.forEach(element => {
						if (insumo.ID_Proceso == element.Sub_ID)
							insumo.ID_Proceso = element.ID_Proceso;
					});
				});

				/* Agregamos los insumos */
				insumos.forEach(insumo => {
					conn('INSUMO').insert({
						Nombre: insumo.Nombre,
						Obligatorio: insumo.Obligatorio,
						Fecha_Entrega: insumo.Fecha_Entrega,
						Tipo: insumo.Tipo,
						Descripcion: insumo.Descripcion
					}).then(idInsumo => {
						conn('PROC_INSUMO').insert({
							ID_Proceso: insumo.ID_Proceso,
							ID_Insumo: idInsumo[0]
						}).then( resultIns => {
							// res.status(200).send({
							// 	resp: "Se ha creado el proceso correctamente"
							// });
						}).catch(error => {
							res.status(500).send({ resp: "Error: " + error });
						});
					}).catch(error => {
						res.status(500).send({ resp: "Error: " + error });
					});
				});

				/* agregamos los niveles */
				conn('LEVELS').insert(levels).then(resultIns => {
				}).catch(error => {
					res.status(500).send({
						Error: "error al insertar los levels" + error
					});
					//agregar el eliminar los datos insertados cen aso de fallo
				});

				/* agregamos los seriales */
				conn('SERIALS').insert(serials).then(resultIns => {
					res.status(200).send({
						resp:  "Se ha agregado el proceso correctamente",
						idMacroProc: id[0]

						//ResultIns: resultIns
					});
				}).catch(error => {
					res.status(500).send({
						error: "error al insertar los serials" + error
					});
					//agregar el eliminar los datos insertados cen aso de fallo
				});
			}).catch(error => {
				res.status(500).send({
					Error: "Error al obtener los id y sub_Id " + error
				});
				//agregar el eliminar los datos insertados cen aso de fallo
			});
		}).catch(error => {
			return res.status(500).send({
				Error: error
				//data: procesos
			});
		});
	});
}


function CreateProceso(req, res) {
	let idPlantilla = req.body.idPlantilla;
	let procesos = req.body.procesos;
	let levels = req.body.levels;
	let serials = req.body.serials;
	let insumos = req.body.insumos;
	let macroProceso = req.body.macroProceso;
	let nombre = req.body.nombreProceso;

	if (idPlantilla > 0) {
		
		/* Primero obtenemos los valores de la plantilla para crear el objeto padre */
		getPlantilla(idPlantilla).then(plantilla =>{
			
			macroProceso = plantilla[0];
			macroProceso.Nombre = nombre;
			
			/* Creamos el proceso a partir de una plantilla */
			InsertAllDataProcess(procesos, levels, serials, macroProceso, res, insumos);

		}).catch(error => {

			res.status(500).send({resp: "Ocurrió un error", Error: error});

		});
		
	}
	else if(idPlantilla == 0) {
		
		/* Creamos el proceso a partir de los datos del macroProceso */
		InsertAllDataProcess(procesos, levels, serials, macroProceso, res, insumos);

	}else{
		res.status(200).send({resp: "Número de plantilla inválida "});
	}
}

/*=====  End of Creamos el porceso a partir de la plantilla seleccionada  ======*/



/*=================================================================
=            Agregamos los Indicadores de los procesos            =
=================================================================*/

function InsertIndicadores(req, res) {
	let idMacroProceso = req.body.idMacroProceso;
	let indicadores = req.body.Indicadores;
	let estrategias = req.body.Estrategias;

	getIdsProc(idMacroProceso).then(result => {
		/* Cambiamos el ID_Proceso temporal que tiene indicadores y ponemos el id de la base */
		indicadores.map((indicador, index) => {

			let indice = result.map(e => { return e.Sub_ID }).indexOf(indicador.ID_Proceso);
			indicador.ID_Proceso = result[indice].ID_Proceso;
			
			conn('INDICADOR').insert(indicador).then(idIndicador => {

				estrategias[index].ID_Indicador = idIndicador;
				conn('ESTRATEGIA').insert(estrategias[index]).then(result => { });

			});

		});

		/* El siguiente for se realiza para comprobar el llenado de los indicadores */
		/*for(let index in indicadores) {
			let indice = result.map(e => { return e.Sub_ID }).indexOf(indicadores[index].ID_Proceso);
			indicadores[index].ID_Proceso = result[indice].ID_Proceso;
			
			conn('INDICADOR').insert(indicadores[index]).then(idIndicador => {
				estrategias[index].ID_Indicador = idIndicador;
				conn('ESTRATEGIA').insert(estrategias[index]).then(result => { });
				// console.log("index: ", index);
				// console.log("indicador: ", indicador);
				// console.log("estrategia: ",  estrategias[index]);
			});
		}*/

		res.status(200).send({ resp: "Datos ingresados correctamente" });
	})
	.catch(err => {
		res.status(500).send({error: "Error: " + err});
	});
}

/*=====  End of Agregamos los Indicadores de los procesos  ======*/


/*============================================================================
=            Método para obtener el JSON del proceso seleccionado            =
============================================================================*/


const getSubProcesos = (idProceso) => {
	return conn('PROCESO').where('ID_Proc_Base', idProceso);
}

const getLevels = (idProceso) => {
	return conn('LEVELS').where('ID_Proceso', idProceso).select('Padre', 'Hijo').orderBy('Padre', 'asc');
}

const getSerials = (idProceso) => {
	return conn('SERIALS').where('ID_Proceso', idProceso).select('ID_Sub_Proceso', 'ID_Seriado').orderBy('ID_Sub_Proceso', 'asc');
}

function getProceso(req, res) {
	let idProceso = req.params.idProceso;
	getSubProcesos(idProceso).then(procesos => {
		let proceso = [];
		proceso.push(procesos);
		getLevels(idProceso).then(levels => {
			proceso.push(levels);
			getSerials(idProceso).then(serials =>{
				proceso.push(serials);
				res.status(200).send({
					procesos: proceso[0],
					levels: proceso[1],
					serials: proceso[2]
				});
			});
		});
	}).catch(error => {
		res.status(500).send({resp:'error', error: `${error}`});
	});
}

/*=====  End of Método para obtener el JSON del proceso seleccionado  ======*/


/*===========================================================
=            Método para obtener el proceso base            =
===========================================================*/

function GetProcesoPrincipal(req, res) {
	
	let idProceso = req.params.idProceso;
	let subQuery = conn('LEVELS').where('ID_Proceso', idProceso).select('Hijo');
	
	conn('PROCESO').where(builder => { builder.whereNotIn('ID_Proceso', subQuery) })
	.andWhere('ID_Proc_Base', idProceso).orderBy('Sub_ID','asc').then(result => {
		res.status(200).send({resp: result});
	}).catch(err => {
		res.status(500).send({resp: {error: "" + err}});
	});

}

/*=====  End of Método para obtener el proceso base  ======*/


/*================================================================
=            Método para obtener todos los pendientes            =
================================================================*/

function Ordenar(procesos, insumos) {

	let pendientes = procesos.concat(insumos);
	pendientes.sort((a,b) => {
		let fechaA = (typeof a.Fecha_Ini !== "undefined")? a.Fecha_Ini : a.Fecha_Entrega;
		let fechaB = (typeof b.Fecha_Ini !== "undefined")? b.Fecha_Ini : b.Fecha_Entrega;
		return new Date(fechaA) - new Date(fechaB);
	});
	return pendientes;

}

function AddResponsable(insumos, procesos) {
	insumos.map( insumo => {
		const idx = procesos.map(e => { return e.ID_Proceso }).indexOf(insumo.ID_Proceso);
		insumo.Nombre = `${ procesos[idx].Nombre } ${ procesos[idx].Ap_Paterno } ${ procesos[idx].Ap_Materno }`;
		insumo.Foto = procesos[idx].Foto;
	});
	return insumos;
}

function GetPendientes(req, res) {

	let idProceso = req.params.idProceso;
	let subQuery = conn(`PROCESO`).where('ID_Proc_Base', idProceso).select('ID_Proceso');

	conn('PROCESO').leftJoin('ROL_PROC', 'ROL_PROC.ID_Proceso', '=', 'PROCESO.ID_Proceso')
	.leftJoin('USUARIO', 'ROL_PROC.Responsable', '=', 'USUARIO.ID_Usuario').where({
		'Completado': 0,
		'ID_Proc_Base': idProceso
	}).select('PROCESO.ID_Proceso', 'PROCESO.Nombre as NombreProc', 'PROCESO.Fecha_Ini', 'PROCESO.Fecha_Fin', 'PROCESO.Duracion', 
		'PROCESO.Obligatorio', 'PROCESO.Costo', 'PROCESO.Completado', 'PROCESO.Descripcion', 'PROCESO.Tipo', 'PROCESO.ID_Proc_Base',
		'USUARIO.ID_Usuario', 'USUARIO.Correo', 'USUARIO.Nombre', 'USUARIO.Ap_Paterno', 'USUARIO.Ap_Materno', 'USUARIO.Foto', 'USUARIO.Color',
		'USUARIO.ID_Empresa')
	.orderBy('Fecha_Ini', 'asc').then( resultProc => {
		// res.status(200).send({ resp: resultProc });
		conn('INSUMO').leftJoin('PROC_INSUMO', 'INSUMO.ID_Insumo', '=', 'PROC_INSUMO.ID_Insumo')
		.whereIn('PROC_INSUMO.ID_Proceso', subQuery ).andWhere( function() {
			this.where('Completado', 0).orWhereNull('Completado');
		}).orderBy('Fecha_Entrega').select('Nombre as NombreProc', 'Fecha_Entrega', 'Obligatorio', 'Tipo', 'Completado', 
			'Descripcion', 'ID_Proceso').then( resultIns => {
			resultIns = AddResponsable(resultIns, resultProc);
			let pendientes = Ordenar(resultProc, resultIns);
			res.status(200).send({ resp: pendientes });
		});
	}).catch(error => {
		res.status(500).send({ resp:'error', error: `${error}` });
	});

}

/*=====  End of Método para obtener todos los pendientes  ======*/


/*=====================================================================
=            Actualizamos el estado de un proceso o insumo            =
=====================================================================*/

function UpdateStatus(req, res) {
	
	let status = req.params.status;
	let idProceso = req.params.idProceso;

	conn('PROCESO').where("ID_Proceso", idProceso).update('Completado', status).then(result => {
		if (result) {
			res.status(200).send({resp: "Estado del proceso Actualizado", actualizado: true});
		}
		else {
			res.status(200).send({resp: "No se actulzizó el estado del porceso", actualizado: false});	
		}
	}).catch(error => {
		res.status(500).send({resp:'error', error: `${error}`});
	});
}

/*=====  End of Actualizamos el estado de un proceso o insumo  ======*/




/*===========================================================================
=            Metódo para obtener tódos los Procesos base creados            =
===========================================================================*/

function GetProcesosBase(req, res) {
	let idEmpresa = req.params.idEmpresa || 0;
	let subQuery = conn('PROC_PLANT').select('ID_Proceso');

	conn('PROCESO').where({
		'Sub_ID': 0, 
		'ID_Empresa': idEmpresa
	}).whereNotIn('ID_Proceso', subQuery).then(result => {
		if(result)
			res.status(200).send({
				resp: result,
				existe: true
			});
		else
			res.status(404).send({resp: "No se ha encontrado ningun proceso activo", existe: false});
	}).catch(error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Metódo para obtener tódos los Procesos base creados  ======*/


/*===========================================================================
=            Obtenemos todos los procesos a partir de un arreglo            =
===========================================================================*/

function GetProcesos (req, res) {
	let idProcBase = req.params.idProcBase || 0;

	getSubProcesos(idProcBase).then( procesos => {
		if( procesos )
			res.status(200).send({ resp: procesos });
		else
			result.status(404).send({ resp: 'error', error: 'No se encontraron los procesos seleccionados' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos todos los procesos a partir de un arreglo  ======*/

/*==============================================================================
=            Obtenemos los ID's de los procesos de un macro proceso            =
==============================================================================*/

function GetIdsProcesos (req, res) {
	let idProcBase = req.params.idProcBase || 0;

	conn('PROCESO').where('ID_Proc_Base', idProcBase).select('ID_Proceso').then( procesos => {
		if( procesos )
			res.status(200).send({ resp: procesos });
		else
			result.status(404).send({ resp: 'error', error: 'No se encontraron los procesos seleccionados' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los ID's de los procesos de un macro proceso  ======*/


/*==============================================================
=            Obtenemos el responsable de un proceso            =
==============================================================*/

function GetResponsable (req, res) {
	let idProceso = req.params.idProceso;

	conn('ROL_PROC').where('ID_Proceso', idProceso).innerJoin('USUARIO', 'ROL_PROC.Responsable', '=', 'USUARIO.ID_Usuario')
	.select('USUARIO.ID_Usuario', 'Nombre', 'Ap_Paterno', 'Ap_Materno', 'Foto', 'Color').then( proceso => {
		if( proceso )
			res.status(200).send({ resp: proceso[0] });
		else
			result.status(404).send({ resp: 'error', error: 'No se encontró Un responsable asigado' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos el responsable de un proceso  ======*/

/*========================================================
=            Agregamos un proceso en creación            =
========================================================*/

function AddCreacionProceso(req, res) {
	let idProceso = req.params.idProceso;

	conn('CREACION_PROCESO_COMPLET').insert({ID_Proceso: idProceso, Completado: false}).then( idCreacion => {
		if( idCreacion )
			res.status(200).send({ resp: idProceso[0] });	
		else
			res.status(500).send({ resp: 'error', error: 'No se logró agregar el status del proceso' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Agregamos un proceso en creación  ======*/

/*===========================================================================
=            actualizamos el estado de la creación de un proceso            =
===========================================================================*/

function UpdateCreacionProceso(req, res) {
	let idProceso = req.params.idProceso;

	conn('CREACION_PROCESO_COMPLET').where('ID_Proceso', idProceso).update({ Completado: true }).then( result => {
		if( result )
			res.status(200).send({ resp: 'creación finalizada', result: result });	
		else
			res.status(500).send({ resp: 'error', error: 'No se logró actualizar el status del proceso' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});	
}

/*=====  End of actualizamos el estado de la creación de un proceso  ======*/

/*===========================================================
=            asignamos los roles de los procesos            =
===========================================================*/

function AddRoles(req, res) {
	let relaciones = req.body.relaciones;
	let asignaciones = [];

	for(let relacion of relaciones) {
		asignaciones.push({ ID_Rol: relacion.idRol, ID_Proceso: relacion.idProceso, Responsable: relacion.responsable });
	}

	conn('ROL_PROC').insert(asignaciones).then( idAsignacion => {
		if( idAsignacion )
			res.status(200).send({ resp: idAsignacion[0] });
		else
			res.status(500).send({ resp: 'error', error: 'No se logró asignar el rol' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of asignamos los roles de los procesos  ======*/


/*=================================================
=            Eliminamos una asignación            =
=================================================*/

function DeleteRol(req, res) {
	let idAsignacion = req.params.idAsignacion;

	conn('ROL_PROC').where('ID_Rol_Proc', idAsignacion).del().then( result => {
		res.status(200).send({ resp: 'Rol elminado', result: result });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Eliminamos una asignación  ======*/

/*===========================================
=            Actualizamos un rol            =
===========================================*/

function UpdateRol(req, res) {
	let idAsignacion = req.params.idAsignacion;
	let asignacion = { 
		ID_Rol: req.body.idRol,
		ID_Proceso: req.body.idProceso,
		Responsable: req.body.responsable
	};

	conn('ROL_PROC').where('ID_Rol_Proc', idAsignacion).update(asignacion).then( result => {
		if( result )
			res.status(200).send({ resp: 'Rol actualizado correctamente', result: result });
		else
			res.status(500).send({ resp: 'error', error: 'no se encontró el rol paraactualizar' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Actualizamos un rol  ======*/



module.exports = {

	AddProceso,
	UpdateProceso,
	DeleteProceso,
	GetDetailProceso,
	ValidarProceso,
	CreateProceso,
	GetProcesosBase,
	InsertIndicadores,
	getProceso,
	GetProcesoPrincipal,
	GetPendientes,
	UpdateStatus,
	GetProcesos,
	GetResponsable,
	AddCreacionProceso,
	UpdateCreacionProceso,
	AddRoles,
	DeleteRol,
	UpdateRol,
	GetIdsProcesos
}