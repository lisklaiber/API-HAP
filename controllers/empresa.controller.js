'use strict'

const conn = require('./connection.js');
let path = require('path');
let Empresa = require('../models/empresa.model.js');



/*=================================================
=            Agregar una Empresa Nueva            =
=================================================*/

function AddEmpresa(req, res){
	let logo = '';
	
	if (req.file) {
	    // console.dir(req.file);
	    logo = req.file.filename;
	}

	let empresa = new Empresa(req.body.nombre, req.body.acronimo, req.body.direccion, 
		logo, req.body.color);
	
	conn('EMPRESA').insert(empresa).then(result => {
		res.status(200).send({ resp: 'Empresa registrada correctamente', file: req.file });
	}).catch(error => {
		res.status(200).send({
			resp: "error",
			Error: `${error}`
		});
	});
}

/*=====  End of Agregar una Empresa Nueva  ======*/


/*===============================================
=            Consulta de una empresa            =
===============================================*/

function GetEmpresa(req, res){
	let idEmpresa = req.params.idEmpresa;

	conn('EMPRESA').where('ID_Empresa', idEmpresa).select().then(empresa => {
		if(!empresa){
			res.status(200).send({ resp: "Empresa no encontrada" })
		}
		res.status(200).send({ resp: "Succes", empresa: empresa[0] });
	}).catch(error => {
		res.status(500).send({resp: "error", error: `${error}`});
	});
}

/*=====  End of Consulta de una empresa  ======*/

/*=======================================================
=            Obtenemos el logo de la empresa            =
=======================================================*/

function GetLogo(req, res){
	let logo = req.params.logo;
	res.sendFile(path.resolve(`./Imgs/Empresa/${logo}`));
}

/*=====  End of Obtenemos el logo de la empresa  ======*/



/*======================================================
=            Constulta de todas las epresas            =
======================================================*/

function GetEmpresas(req, res){
	conn('EMPRESA').select().then(allEmpresas => {
		res.send( allEmpresas );
	});
}

/*=====  End of Constulta de todas las epresas  ======*/

/*================================================================
=            Actualización de los datos de la empresa            =
================================================================*/

function UpdateEmpresa(req, res){
	let idEmpresa = req.params.idEmpresa;
	let empresa = new Empresa(req.body.nombre, req.body.acronimo, req.body.direccion, 
		req.body.logo, req.body.color);

	conn('EMPRESA').where('ID_Empresa', idEmpresa).update(empresa).then(result => {
		if (result != 0) {
			res.status(200).send({
				resp: "Datos actualizados correctamente",
				data: result
			});
		}
		else{
			res.status(500).send({ resp: "No se actualizó ninguna empresa" });
		}
	})
	.catch(error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Actualización de los datos de la empresa  ======*/


/*==================================================
=            Eliminación de una empresa            =
==================================================*/

function DeleteEmpresa(req, res){
	let idEmpresa = req.params.idEmpresa;

	conn('PLANTILLA').where('ID_Empresa', idEmpresa).del().then(result => {
		if(result)
			res.status(200).send({ resp: result });
		else
			res.status(200).send({ resp: 'error al eliminar' });
	}).catch(error => {
		res.stauts(500).send({ resp : 'error', error: `${error}` });
	});
}

/*=====  End of Eliminación de una empresa  ======*/



module.exports = {
	AddEmpresa,
	GetEmpresa,
	GetLogo,
	GetEmpresas,
	UpdateEmpresa,
	DeleteEmpresa
}