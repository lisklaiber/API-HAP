'use strict'

const CONN = require('./connection.js');
let Indicador = require('../models/indicador.model.js');


/*==============================================
=            Agregamos un indicador            =
==============================================*/

function AddIndicador (req, res){
	let tipo = req.params.tipo;
	let table = '';
	let indicador;

	if(tipo < 3 && tipo > 0){
		/* Instanciamos el indicador */		
		if (tipo == 1) {
			indicador = new Indicador.IndicadorComp(req.body.nombre, req.body.verdeMax, req.body.verdeMin, 
				req.body.amMax, req.body.amMin, req.body.rojo, req.body.descripcion, req.body.idProceso);
			table = 'INDICADOR_COMP';
		} else if (tipo == 2){
			indicador = new Indicador.IndicadorTC(req.body.nombre, req.body.tipo, req.body.verdeMax,
				req.body.verdeMin, req.body.amSupMax, req.body.amSupMin, req.body.amInfMax, req.body.amInfMin,
				req.body.rojoSup, req.body.rojoInf, req.body.descripcion, req.body.idProceso);
			table = 'INDICADOR';
		}

		CONN(`${table}`).insert(indicador).then( idIndicador => {
			if( idIndicador )
				res.status(200).send({ resp: 'Indicador agregado correctamente', id: idIndicador[0] });
			else
				res.status(500).send({ resp: 'error', error: 'No se pudo agregar el indicador', indicador: indicador });
		}).catch( error => {
			res.status(500).send({ resp: 'error', error: `${error}`, indicador: indicador });
		});
	}
	else
		res.status(409).send({ resp: 'El tipo de indicador seleccionado no es válido' });

}

/*=====  End of Agregamos un indicador  ======*/


/*=================================================
=            Actualizamos un indicador            =
=================================================*/

function UpdateIndicador (req, res){
	let tipo = req.params.tipo;
	let idIndicador = req.params.idIndicador;
	let table = '';
	let indicador;

	if(tipo < 3 && tipo > 0){
		/* Instanciamos el indicador */		
		if (tipo == 1) {
			indicador = new Indicador.IndicadorComp(req.body.nombre, req.body.verdeMax, req.body.verdeMin, 
				req.body.amMax, req.body.amMin, req.body.rojo, req.body.descripcion, req.body.idProceso);
			table = 'INDICADOR_COMP';
		} else if (tipo == 2){
			indicador = new Indicador.IndicadorTC(req.body.nombre, req.body.tipo, req.body.verdeMax,
				req.body.verdeMin, req.body.amSupMax, req.body.amSupMin, req.body.amInfMax, req.body.amInfMin,
				req.body.rojoSup, req.body.rojoInf, req.body.descripcion, req.body.idProceso);
			table = 'INDICADOR';
		}

		CONN(`${table}`).where('ID_Indicador', idIndicador).update(indicador).then( result => {
			if( result )
				res.status(200).send({ resp: 'Indicador actualizado correctamente', result: result });
			else
				res.status(500).send({ resp: 'error', error: 'No se pudo actualizar el indicador' });
		}).catch( error => {
			res.status(500).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(409).send({ resp: 'El tipo de indicador seleccionado no es válido' });
}

/*=====  End of Actualizamos un indicador  ======*/


/*===========================================================
=            Obtenemos los datos de un indicador            =
===========================================================*/

function GetIndicador (req, res){
	let tipo = req.params.tipo;
	let idIndicador = req.params.idIndicador;
	let table = '';

	if(tipo < 3 && tipo > 0){
		
		/* Instanciamos el indicador */		
		if (tipo == 1) 
			table = 'INDICADOR_COMP';
		else if (tipo == 2)
			table = 'INDICADOR';
		

		CONN(`${table}`).where('ID_Indicador', idIndicador).then( indicador => {
			if( indicador )
				res.status(200).send({ resp: indicador });
			else
				res.status(404).send({ resp: 'error', error: 'No se encontró el indicador' });
		}).catch( error => {
			res.status(500).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(409).send({ resp: 'El tipo de indicador seleccionado no es válido' });
}

/*=====  End of Obtenemos los datos de un indicador  ======*/


/*===============================================================
=            Obtenemos los indicadores de un proceso            =
===============================================================*/

function GetIndicadores (req, res){
	let idProceso = req.params.idProceso;		

	CONN(`INDICADOR_COMP`).where('ID_Proceso', idProceso).orderBy('ID_Proceso', 'ASC').then( indicadorComp => {
		CONN(`INDICADOR`).where({ 'ID_Proceso': idProceso, 'Tipo': 1 }).orderBy('ID_Proceso', 'ASC').then( indicadorT => {
			CONN(`INDICADOR`).where({ 'ID_Proceso': idProceso, 'Tipo': 2 }).orderBy('ID_Proceso', 'ASC').then( indicadorC => {
				res.status(200).send({ 
					indicadorComp: indicadorComp[0],
					indicadorT: indicadorT[0],
					indicadorC: indicadorC[0]
				});
			});
		});
	});
}

/*=====  End of Obtenemos los indicadores de un proceso  ======*/


/*===============================================
=            Eliminamos un indicador            =
===============================================*/

function DeleteIndicador(req, res){
	let tipo = req.params.tipo;
	let idIndicador = req.params.idIndicador;
	let table = '';

	if(tipo < 3 && tipo > 0){
		/* Instanciamos el indicador */		
		if (tipo == 1) {
			table = 'INDICADOR_COMP';
		} else if (tipo == 2){
			table = 'INDICADOR';
		}

		CONN(`${table}`).where('ID_Indicador', idIndicador).del().then( result => {
			res.status(200).send({ resp: 'Indicador eliminado', result: result });
		}).catch( error => {
			res.status(500).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(409).send({ resp: 'El tipo de indicador seleccionado no es válido' });
}

/*=====  End of Eliminamos un indicador  ======*/

/*================================================================
=            Eliminamos los indicadores de un proceso            =
================================================================*/

function DeleteIndicadores (req, res){
	
	let idProceso = req.params.idProceso;

	CONN(`INDICADOR_COMP`).where('ID_Proceso', idProceso).del().then().catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
	CONN(`INDICADOR`).where('ID_Proceso', idProceso).del().then( result => {
		res.status(200).send({ resp: 'Datos eliminados', result: result });
	}).catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
	
}

/*=====  End of Eliminamos los indicadores de un proceso  ======*/



module.exports = {
	AddIndicador,
	UpdateIndicador,
	GetIndicador,
	GetIndicadores,
	DeleteIndicador,
	DeleteIndicadores
}