'use strict'
const conn = require('./connection.js');
var User = require('../models/autentication.js');

/*====================================================================
=            Funcion para la aautentificación del usuario            =
====================================================================*/

function Autentication(req, res){
	let user = User.create(req.body.correo, req.body.pass);
	conn('USUARIO').where('Correo', user.correo).select('Pass').then(pass => {
		if(user.pass == pass[0].Pass)
			res.send({resp: true});
		else
			res.send({resp: false});
	}).catch(error => {
		res.status(500).send({resp: "Usuario no encontrado", error: `${error}`});
	});
}


/*=====  End of Funcion para la aautentificación del usuario  ======*/


/*===============================================================================
=            Función para guardar el acceso realizado por el usuario            =
===============================================================================*/

function Acceso(req, res){
	conn('ACCESOS').insert({
		ID_Usuario: req.body.idUsuario,
		Fecha: req.body.fecha
	}).then(result => {
		res.status(200).send({
			resp: "Acceso registrado"
		});
	}).catch(error => {
		res.status(200).send({ resp: "Acceso no registrado", error: `${error}` });
	});
}

/*=====  End of Función para guardar el acceso realizado por el usuario  ======*/



module.exports = {
	Autentication,
	Acceso
}