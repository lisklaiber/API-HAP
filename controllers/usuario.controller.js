'use strict'
const conn = require('./connection.js');
let path = require('path');
let Usuario = require('../models/usuario.model.js');


/*======================================
=            Validar Correo            =
======================================*/

function ValidarCorreo(req, res){
	//let correo = req.params.correo;
	let correo = req.body.correo;
	conn('USUARIO').where('Correo', correo).select('Correo').then(result => {
		if(result[0])
			res.send({ resp: "El correo ya existe" });
		else
			res.send({ resp: "Aprobado" });
	})
	.catch(error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Validar Correo  ======*/


/*====================================================
=            Funcion para agregar usuario            =
====================================================*/

function AddUsuario(req, res){
	let foto = "";

	if (req.file) {
	    // console.dir(req.file);
	    foto = req.file.filename;
	}

	let usuario = new Usuario(req.body.correo, req.body.pass, req.body.nombre, req.body.apPaterno, 
		req.body.apMaterno, foto, req.body.color, req.body.idEmpresa);
	conn('USUARIO').insert(usuario).then(result => {
		res.status(200).send({
			// succes: true, 
			resp: "Usuario guardado correctamente",
			data: result[0],
			req: req.body,
			file: req.file

		});
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}`, req: req.body });
	});
	/* Registramos la accion en el histórico */
	
}

/*=====  End of Funcion para agregar usuario  ======*/


/*=========================================
=            Update de usuario            =
=========================================*/

function UpdateUsuario(req, res){
	let idUsuario = req.params.idUsuario;
	let user = new Usuario(req.body.correo, req.body.pass, req.body.nombre, req.body.apPaterno, 
		req.body.apMaterno, req.body.foto, req.body.color, req.body.idEmpresa);
	conn('USUARIO').where('ID_Usuario', idUsuario).update(user).then(result => {
		if (result != 0) {
			res.status(200).send({
				// succes: true, 
				resp: "Usuario actualizado correctamente",
				data: result
			});
		}
		else{
			res.status(500).send({
				resp: "No se actualizó ningun usuario"
			});
		}
	})
	.catch(error => {
		res.status(500).send({
			resp: "Error al actualizar el usuario",
			error: `${error}`,
			user: user,
			params: req.body
		});
	});
}

/*=====  End of Update de usuario  ======*/


/*==========================================
=            Cambio de password            =
==========================================*/

function ChangePass(req, res){
	let idUsuario = req.params.idUsuario;
	let password = req.body.pass;

	conn('USUARIO').where('ID_Usuario', idUsuario).update({
		Pass: password
	}).then(result => {
		if (result != 0) {
			res.status(200).send({
				// succes: true, 
				resp: "Password actualizado correctamente"
				//resultado: result
			});
		}
		else{
			res.status(500).send({
				resp: "El password no cumple con los requsitos"
			});
		}
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});
}

/*=====  End of Cambio de password  ======*/


/*========================================================================
=            Obteneción de los todos los usuarios para editer            =
========================================================================*/

function GetUsuarios(req, res){
	let idEmpresa = req.params.idEmpresa;
	conn('USUARIO').where('ID_Empresa', idEmpresa).then(usuarios => {
		if (!usuarios) {
			res.status(200).send({
				resp: "No se encontró ningun usuario"
			})
		}
		else{
			res.status(200).send({
				resp: usuarios
				//Usuarios: usuarios
			});
		}
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obteneción de los todos los usuarios para editer  ======*/

/*=========================================================
=            Obtenemos los roles de un usuario            =
=========================================================*/

function GetRoles(req, res){
	let idUsuario = req.params.idUsuario;

	conn('USER_ROL').leftJoin('ROL', 'USER_ROL.ID_Rol', '=', 'ROL.ID_Rol').where('ID_Usuario', idUsuario)
	.select('ROL.ID_Rol','ROL.Nombre', 'ROL.Descripcion', 'ROL.Icono', 'ROL.Color').then( roles => {
		if (!roles) {
			res.status(200).send({
				resp: "Este usuario no tiene roles asignados"
			})
		}
		else{
			res.status(200).send({
				resp: roles
				//Usuarios: usuarios
			});
		}
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los roles de un usuario  ======*/


/*========================================
=            Cambio de imagen            =
========================================*/

function ChangeFoto(req, res){
	let idUsuario = req.params.idUsuario;
	if (req.file) {
	    // console.dir(req.file);
	    if (req.body.nombreFoto == '') {
	    	let filename = req.file.filename;
	    	conn('USUARIO').where('ID_Usuario', idUsuario).update({Foto: filename}).then();
	  	}
	  	res.send({resp: 'Cambio realizado'});
	}
}

/*=====  End of Cambio de imagen  ======*/



/*==========================================================
=            Obtenemos un usuario en específico            =
==========================================================*/

function GetUsuario(req, res){
	let idUsuario = req.params.idUsuario;

	conn('USUARIO').where('ID_Usuario', idUsuario).select().then(usuario => {
		if(!usuario[0]){
			res.status(200).send({
				resp:"faild", 
				error: "Usuario no encontrado"
			});
		}
		else{
			res.status(200).send({
				resp: usuario[0]
			});
		}
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});

}

/*=====  End of Obtenemos un usuario en específico  ======*/



/*==================================================
=            Elimincación de un usuario            =
==================================================*/

function DeleteUsuario(req, res){
	let idUsuario = req.params.idUsuario;

	conn('')

	conn('USUARIO').where('ID_Usuario', idUsuario).del().then( result => {
		if(result == 1)
			res.status(200).send({resp: result});
		else
			res.status(200).send({resp: 'error al eliminar'});
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});
}

/*=====  End of Elimincación de un usuario  ======*/


/*========================================================
=            obtención de la foto del usuario            =
========================================================*/

function GetFoto(req, res){
	let foto = req.params.foto;
	res.sendFile(path.resolve(`./Imgs/Usuario/${foto}`));
}

/*=====  End of obtención de la foto del usuario  ======*/


/*===================================================
=            asignamos un rol al usuario            =
===================================================*/

function AddRol(req, res) {
	const idUsuario = req.body.idUsuario;
	const idRoles = req.body.idRoles;
	let relaciones = [];

	for(let idRol of idRoles){
		relaciones.push({ ID_Usuario: idUsuario, ID_Rol: idRol });
	}


	conn('USER_ROL').insert(relaciones).then( idRelacion => {
		if( idRelacion )
			res.status(200).send({ resp: idRelacion });
		else
			res.status(500).send({resp: 'error', error: 'Fallo al insertar la relación' });
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});
}

/*=====  End of asignamos un rol al usuario  ======*/



/*==========================================================
=            Asignamos un procesos a un usuario            =
==========================================================*/

function AsignarProceso(req, res){
	let idUsuario = req.body.idUsuario;
	let idProceso = req.body.idProceso;
	let responsable = req.body.responsable;

	conn('USER_PROC').insert({
		ID_Usuario: idUsuario,
		ID_Proceso: idProceso,
		Responsable: responsable
	}).then( idRelacion => {
		if( idRelacion )
			res.status(200).send({ resp: idRelacion });
		else
			res.status(500).send({ resp:'error', error: 'No se pudo insertar la relacion' });
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});
}

/*=====  End of Asignamos un procesos a un usuario  ======*/


/*====================================================================
=            Obtenemos los id's de los procesos asignados            =
====================================================================*/

function GetProcesos(req, res){
	let idUsuario = req.params.idUsuario;

	conn('USER_PROC').select('ID_Proceso').then( idsProcesos => {
		if(idsProcesos)
			res.status(200).send({ resp: idsProcesos });
		else
			res.status(404).send({ resp:'El usuario no tiene ningun proceso asignado' });
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});

}

/*=====  End of Obtenemos los id's de los procesos asignados  ======*/



/*==================================================
=            Exportación de los métodos            =
==================================================*/

module.exports = {
	ValidarCorreo,
	AddUsuario,
	UpdateUsuario,
	ChangePass,
	ChangeFoto,
	GetUsuarios,
	GetRoles,
	DeleteUsuario,
	GetUsuario,
	GetFoto,
	AddRol,
	AsignarProceso,
	GetProcesos
}

/*=====  End of Exportación de los métodos  ======*/