'use strict'

const CONN = require('./connection.js');
let Estrategia = require('../models/estrategia.model.js');

/*================================================
=            Agregamos una estrategia            =
================================================*/

function AddEstrategia(req, res){
	let tipo = req.params.tipo;
	let estrategia = new Estrategia(req.body.nombre, req.body.descripcion, req.body.idIndicador, req.body.semaforo);
	if(tipo < 3 && tipo > 0){
		let table = '';
		if(tipo == 1)
			table = 'ESTRATEGIA_COMP';
		else
			table = 'ESTRATEGIA';

		CONN(`${table}`).insert(estrategia).then( idEstrategia =>{
			if( idEstrategia )
				res.status(200).send({ resp: 'Estrategia agregada', id: idEstrategia });
			else
				res.status(500). send({ resp: 'error', error: 'No se pudo agregar la estrategia' });
		}).catch(error => {
			res.status(200).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(500).send({ resp: 'error', error: 'El tipo de estrategia seleccionado es inválido' });
}

/*=====  End of Agregamos una estrategia  ======*/


/*===================================================
=            Actualizamos una estrategia            =
===================================================*/

function UpdateEstrategia(req, res){
	let tipo = req.params.tipo;
	let idEstrategia = req.params.idEstrategia;
	let estrategia = new Estrategia(req.body.nombre, req.body.descripcion, req.body.idIndicador, req.body.semaforo);

	if(tipo < 3 && tipo > 0){
		let table = '';
		if(tipo == 1)
			table = 'ESTRATEGIA_COMP';
		else
			table = 'ESTRATEGIA';

		CONN(`${table}`).where('ID_Estrategia', idEstrategia).update(estrategia).then( result =>{
			if( result )
				res.status(200).send({ resp: 'Estrategia actualizada', result: result });
			else
				res.status(500). send({ resp: 'error', error: 'No se pudo agregar la estrategia' });
		}).catch(error => {
			res.status(200).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(500).send({ resp: 'error', error: 'El tipo de estrategia seleccionado es inválido' });
}

/*=====  End of Actualizamos una estrategia  ======*/


/*================================================
=            Obtenemos uns estrategia            =
================================================*/

function GetEstrategia(req, res){
	let tipo = req.params.tipo;
	let idEstrategia = req.params.idEstrategia;

	if(tipo < 3 && tipo > 0){
		let table = '';
		if(tipo == 1)
			table = 'ESTRATEGIA_COMP';
		else
			table = 'ESTRATEGIA';

		CONN(`${table}`).where('ID_Estrategia', idEstrategia).then( estrategia =>{
			if( estrategia )
				res.status(200).send({ resp: estrategia[0] });
			else
				res.status(500). send({ resp: 'error', error: 'No se encontró la estrategia' });
		}).catch(error => {
			res.status(200).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(500).send({ resp: 'error', error: 'El tipo de estrategia seleccionado es inválido' });
}

/*=====  End of Obtenemos uns estrategia  ======*/

/*================================================================
=            obtenemos las estrategias de un idicador            =
================================================================*/

function GetEstrategias(req, res){
	let tipo = req.params.tipo;
	let idIndicador = req.params.idIndicador;

	if(tipo < 3 && tipo > 0){
		let table = '';
		if(tipo == 1)
			table = 'ESTRATEGIA_COMP';
		else
			table = 'ESTRATEGIA';

		CONN(`${table}`).where('ID_Indicador', idIndicador).orderBy('Semaforo', 'asc').then( estrategias =>{
			if( estrategias )
				res.status(200).send({ resp: estrategias });
			else
				res.status(500). send({ resp: 'error', error: 'No se encontró la estrategia' });
		}).catch(error => {
			res.status(200).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(500).send({ resp: 'error', error: 'El tipo de estrategia seleccionado es inválido' });
}

/*=====  End of obtenemos las estrategias de un idicador  ======*/


/*=================================================
=            Eliminamos una estrategia            =
=================================================*/

function DeleteEstrategia(req, res){
	let tipo = req.params.tipo;
	let idEstrategia = req.params.idEstrategia;

	if(tipo < 3 && tipo > 0){
		let table = '';
		if(tipo == 1)
			table = 'ESTRATEGIA_COMP';
		else
			table = 'ESTRATEGIA';

		CONN(`${table}`).where('ID_Estrategia', idEstrategia).del().then( result =>{
			res.status(200).send({ resp: 'estrategia eliminada', result: result });
		}).catch(error => {
			res.status(200).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(500).send({ resp: 'error', error: 'El tipo de estrategia seleccionado es inválido' });
}

/*=====  End of Eliminamos una estrategia  ======*/


/*=======================================================================
=            Eliminamos todas las estratigas de un indicador            =
=======================================================================*/

function DeleteEstrategias(req, res){
	let tipo = req.params.tipo;
	let idIndicador = req.params.idIndicador;

	if(tipo < 3 && tipo > 0){
		let table = '';
		if(tipo == 1)
			table = 'ESTRATEGIA_COMP';
		else
			table = 'ESTRATEGIA';

		CONN(`${table}`).where('ID_Indicador', idIndicador).del().then( result =>{
			res.status(200).send({ resp: 'estrategias eliminada', result: result });
		}).catch(error => {
			res.status(200).send({ resp: 'error', error: `${error}` });
		});
	}
	else
		res.status(500).send({ resp: 'error', error: 'El tipo de estrategia seleccionado es inválido' });
}

/*=====  End of Eliminamos todas las estratigas de un indicador  ======*/




module.exports = {
	AddEstrategia,
	UpdateEstrategia,
	GetEstrategia,
	GetEstrategias,
	DeleteEstrategia,
	DeleteEstrategias
}