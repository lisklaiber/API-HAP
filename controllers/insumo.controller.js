'use stric'

const CONN = require('./connection.js');
let Insumo = require('../models/insumo.model.js');


/*===========================================
=            Agregamos un insumo            =
===========================================*/

function AddInsumo (req, res) {
	let insumo = new Insumo(req.body.nombre, req.body.fechaEntrega, req.body.obligatorio, req.body.tipo, 
		req.body.completado,req.body.descripcion, req.body.archivo, req.body.costo);

	CONN('INSUMO').insert(insumo).then( idInsumo => {
		if( idInsumo )
			res.status(200).send({ resp: idInsumo });
		else
			res.status(500).send({error: 'no se pudireon ingresar los datos'});
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Agregamos un insumo  ======*/

/*=========================================================================================
=            Agregamos la relación de un insumo dependiendo del tipo de objeto            =
=========================================================================================*/

function AddRelInsumo(req, res){
	let idObjeto = req.body.idObjeto;
	let idInsumo = req.body.idInsumo;
	let tipo = req.body.tipo || 1;
	let base = 'PROC_INSUMO';
	let id = 'ID_Proceso';

	if(tipo == 2){
		base = 'EVENTO_INSUMO';
		id = 'ID_Evento';
	}
	else if (tipo == 3){
		base = 'DECISION_INSUMO';
		id = 'ID_Decision';
	}

	CONN(`${base}`).insert({ [id]: idObjeto, ID_Insumo: idInsumo }).then( idRelación => {
		if( idRelación )
			res.status(200).send({ resp: idRelación, tipo: tipo, base: base });
		else
			res.status(500).send({error: 'no se pudireon ingresar los datos'});
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Agregamos la relación de un insumo dependiendo del tipo de objeto  ======*/


/*====================================
=            UpdateInsumo            =
====================================*/

function UpdateInsumo(req, res){
	let idInsumo = req.params.idInsumo;

	let insumo = new Insumo(req.body.nombre, req.body.fechaEntrega, req.body.obligatorio, req.body.tipo, 
		req.body.completado,req.body.descripcion, req.body.archivo, req.body.costo);

	CONN('INSUMO').where('ID_Insumo', idInsumo).update(insumo).then( result => {
		if( idInsumo )
			res.status(200).send({ resp: idInsumo });
		else
			res.status(500).send({ error: 'No se pudireon actualizar los datos' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of UpdateInsumo  ======*/

/*=================================
=            GetInsumo            =
=================================*/

function GetInsumo(req, res){
	let idInsumo = req.params.idInsumo;

	CONN('INSUMO').where('ID_Insumo', idInsumo).then( insumo => {
		if( insumo )
			res.status(200).send({ resp: insumo[0] });
		else
			res.status(500).send({ error: 'No se encontró el insumo' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of GetInsumo  ======*/

/*=========================================================================
=            Obtenemos los insumos de un proceso en específico            =
=========================================================================*/

function GetInsumos(req, res){
	let idProceso = req.params.idProceso;
	let tipo = req.params.tipo || 1;
	let base = 'PROC_INSUMO';
	let id = 'ID_Proceso';
	
	if(tipo == 2){
		base = 'EVENTO_INSUMO';
		id = 'ID_Evento';
	}
	else if (tipo == 3){
		base = 'DECISION_INSUMO';
		id = 'ID_Decision';
	}

	CONN('INSUMO').select('INSUMO.ID_Insumo', 'Nombre', 'Fecha_Entrega', 'Obligatorio', 'Tipo', 'Completado', 'Descripcion', 
		'Archivo', 'Costo').join(`${base}`, {'INSUMO.ID_Insumo': `${base}.ID_Insumo`})
	.where(`${base}.${id}`, idProceso).then( insumos => {
		if( insumos )
			res.status(200).send({ resp: insumos });
		else
			res.status(500).send({ error: 'No se encontró ningun insumo' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos los insumos de un proceso en específico  ======*/

/*============================================
=            eliminamos un insumo            =
============================================*/

function DeleteInsumo(req, res){
	let idInsumo = req.params.idInsumo;

	CONN('INSUMO').where('ID_Insumo', idInsumo).del().then( insumo => {
		res.status(200).send({ resp: 'Insumo eliminado' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of eliminamos un insumo  ======*/

/*=====================================================================
=            eliminamos los insumos a partir de un arreglo            =
=====================================================================*/

function DeleteInsumos(req, res){
	let idsInsumos = req.body.idsInsumos;

	CONN('INSUMO').whereIn('ID_Insumo', idsInsumos).del().then( result => {
		res.status(200).send({ resp: 'Insumos eliminado' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of eliminamos los insumos a partir de un arreglo  ======*/


/*===============================================================
=            Eliminamos las rlaciones de los insumos            =
===============================================================*/

function DeleteRelInsumos(req, res){
	let idObjeto = req.params.idObjeto;
	let tipo = req.params.tipo || 1;
	let base = 'PROC_INSUMO';
	let id = 'ID_Proceso';

	if(tipo == 2){
		base = 'EVENTO_INSUMO';
		id = 'ID_Evento';
	}
	else if (tipo == 3){
		base = 'DECISION_INSUMO';
		id = 'ID_Decision';
	}

	CONN(`${base}`).where(`${id}`, idObjeto).del().then( result => {
		res.status(200).send({ resp: 'Relaciones eliminadas', result: result });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Eliminamos las rlaciones de los insumos  ======*/


/*=====================================================
=            Subimos un arcchivo al insumo            =
=====================================================*/

function UploadFile(req, res){
	let idInsumo = req.params.idInsumo;
	let archivo = req.body.archivo || '';
	
	for(let file of req.files){
		archivo += file.filename + ';';
	}

	CONN('INSUMO').where('ID_Insumo', idInsumo).update({ Archivo: archivo }).then( result => {
		if( idInsumo )
			res.status(200).send({ resp: idInsumo, archivos: archivo });
		else
			res.status(500).send({ error: 'No se pudireon actualizar los datos' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});

}

/*=====  End of Subimos un arcchivo al insumo  ======*/


/*======================================================================
=            Obtenemos el archivo al que se hace referencia            =
======================================================================*/

function GetFile(req, params){
	let file = req.params.file;
	res.sendFile(path.resolve(`./Uploads/${file}`));
}

/*=====  End of Obtenemos el archivo al que se hace referencia  ======*/




module.exports = {
	AddInsumo,
	AddRelInsumo,
	UpdateInsumo,
	GetInsumo,
	GetInsumos,
	DeleteInsumo,
	DeleteInsumos,
	DeleteRelInsumos,
	UploadFile,
	GetFile
}