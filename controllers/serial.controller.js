'use strict'

const CONN = require('./connection.js');
let Serial = require('../models/serial.model.js');

/*===================================================
=            Agregamos una serialización            =
===================================================*/

function AddSerial (req, res) {
	let serial = new Serial(req.body.idProceso, req.body.idSubProceso, req.body.idSeriado, req.body.tipoSubProceso, req.body.tipoSeriado);

	CONN('SERIALS').insert(serial).then( idSerial => {
		if( idSerial )
			res.status(200).send({ resp: idSerial });
		else
			res.status(500).send({ resp: 'error', error: 'Ocurrió un porblema. No se insertaron los datos' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Agregamos una serialización  ======*/


/*============================================================
=            Actualizamos los datos de un seriado            =
============================================================*/

function UpdateSerial(req, res){
	let idSerial = req.params.idSerial;
	let serial = new Serial(req.body.idProceso, req.body.idSubProceso, req.body.idSeriado, req.body.tipoSubProceso, req.body.tipoSeriado);

	CONN('SERIALS').where('ID_Serial', idSerial).update(serial).then(result => {
		if(result)
			res.status(200).send({ resp: 'Datos actualizados correctamente' });
		else
			res.status(404).send({ resp: 'error', error: 'No se encontró el serial para actualizar' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Actualizamos los datos de un seriado  ======*/


/*========================================================
=            Obtenemos los datos de un serial            =
========================================================*/

function GetSerial(req, res){
	let idSerial = req.params.idSerial;
	CONN('SERIALS').where('ID_Serial', idSerial).then(serial => {
		if(serial)
			res.status(200).send({ resp: serial });
		else
			res.status(404).send({ resp:'error', error: 'No se encontró el serial' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos los datos de un serial  ======*/


/*======================================================================
=            Obtenemos todos los serials de un proceso base            =
======================================================================*/

function GetSerialsProceso(req, res){
	let idProceso = req.params.idProceso;
	CONN('SERIALS').where('ID_Proceso', proceso).then(serials => {
		if(serials)
			res.status(200).send({ resp: serials });
		else
			res.status(404).send({ resp:'error', error: 'No se encontraron procesos seriados' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos todos los serials de un proceso base  ======*/


/*==============================================================
=            Obtenemos todas las ramas de un serial            =
==============================================================*/

function GetSerials(req, res){
	let idProceso = req.params.idProceso;
	let tipo = req.params.tipo;
	CONN('SERIALS').where({ 'ID_Sub_Proceso': idProceso, 'Tipo_Sub_Proceso': tipo }).then(serials => {
		if(serials)
			res.status(200).send({ resp: serials });
		else
			res.status(404).send({ resp:'error', error: 'No se encontraron procesos seriados' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos todas las ramas de un serial  ======*/


/*====================================================
=            Eliminamos una serialización            =
====================================================*/

function DeleteSerial(req, res){
	let idSerial = req.params.idSerial;
	CONN('SERIALS').where('ID_Serial', idSerial).del().then(serial => {
		res.status(200).send({ resp: 'Serial eliminado correctamente' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Eliminamos una serialización  ======*/


/*===============================================================================
=            Eliminamos todos los serial des un proceso en espcífico            =
===============================================================================*/

function DeleteSerials(req, res){
	let idProceso = req.params.idProceso;
	CONN('SERIAL').where('ID_Sub_Proceso', idProceso).del().then(serial => {
		res.status(200).send({ resp: 'Serials eliminados correctamente' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Eliminamos todos los serial des un proceso en espcífico  ======*/




module.exports = {
	AddSerial,
	UpdateSerial,
	GetSerial,
	GetSerialsProceso,
	GetSerials,
	DeleteSerial,
	DeleteSerials
}