'use strict'

const conn = require('./connection.js');
let Rol = require('../models/rol.model.js');

/*==========================================
=            Validar nombre Rol            =
==========================================*/

function ValidarRol(req, res) {
	let nombre = req.params.nombre;
	conn('ROL').where('Nombre', nombre).select('Nombre').then(result => {
		if ( result[0] ) {
			res.send({resp: "El nombre del Rol ya existe"});
		} else {
			res.send({resp: "Aprobado"});
		}
	})
	.catch(error => {
		res.status(500).send({resp: "error", error: `${error}`});
	});
}

/*=====  End of Validar nombre Rol  ======*/


/*=========================================
=            INSERTAMOS EL ROL            =
=========================================*/

/* Insertamos los permisos del rol */
function InsertPermisos (idRol) {
	let relacion = [];
	conn('PERMISOS').select('ID_Permiso').then( idsPermisos => {

		for(let id of idsPermisos) {
			relacion.push({
				ID_Rol: idRol, 
				ID_Permiso: id.ID_Permiso,
				Estado: false
			});
		}
		// console.log(relacion);
		conn('ROL_PERMISO').insert(relacion).then();
	});

}


function AddRol(req, res) {
	
	let rol = new Rol(req.body.descripcion, req.body.nombre, req.body.color, req.body.icono, 
		req.body.idEmpresa);

	conn('ROL').insert(rol).then(idRol => {
		InsertPermisos(idRol[0]);
		if (!idRol) {
			res.status(200).send({resp: 'error', error: 'Error desconocido'});
		} else {
			res.status(200).send({resp: 'Correcto', idRol: idRol[0]});
		}
	}).catch(error => {
		res.status(500).send({resp: 'error', error: `${error}` });
	});

}

/*=====  End of INSERTAMOS EL ROL  ======*/


/*==================================
=            Update Rol            =
==================================*/

function UpdateRol (req, res) {
	let idRol = req.params.idRol;
	let rol = new Rol(req.body.descripcion, req.body.nombre, req.body.color, req.body.icono, 
		req.body.idEmpresa);

	conn('ROL').where('ID_Rol', idRol).update(rol).then(rowModified =>{
		if ( !idRol ) {
			res.status(200).send({resp: 'fallido', error: 'Error desconocido...'});
		} else {
			res.status(200).send({resp: 'correcto', rowModified: rowModified});
		}
	}).catch(error => {
		res.status(500).send({resp: 'fallido', error: `${error}` });
	});
}

/*=====  End of Update Rol  ======*/


/*=============================================
=            Eliminación de un Rol            =
=============================================*/

function DeleteRol(req, res) {
	let idRol = req.params.idRol;

	conn('ROL_PERMISO').where('ID_Rol', idRol).del().then().catch(error => {
		res.status(500).send({resp: 'fallido', error: `${error}` });
	});

	conn('ROL').where('ID_Rol', idRol).del().then( result => {
		if ( result == 1 ) {
			res.status(200).send({resp: result});
		} else {
			res.status(200).send({resp: 'error al eliminar'});
		}
	}).catch(error => {
		res.status(500).send({resp: 'fallido', error: `${error}` });
	});
}

/*=====  End of Eliminación de un Rol  ======*/


/*=====================================================
=            Obtenemos los datos de un rol            =
=====================================================*/

function GetRol(req, res) {
	let idRol = req.params.idRol;

	conn('ROL').where('ID_Rol', idRol).then( rol => {
		if ( rol ) {
			res.status(200).send({ resp: rol });
		} else {
			res.status(404).send({ resp: 'error', error: 'No se encontró el rol' });
		}
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los datos de un rol  ======*/



/*==========================================================
=            Obtenemos los roles del la empresa            =
==========================================================*/

function GetRoles(req, res) {
	let idEmpresa = req.params.idEmpresa;

	conn('ROL').where('ID_Empresa', idEmpresa).then(result =>{
		res.status(200).send({ resp: result	});
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los roles del la empresa  ======*/


/*=============================================================
=            actualizamos los permisosde los roles            =
=============================================================*/

function UpdatePermisos(req, res) {
	let idRelacion = req.params.idRelacion;

	conn('ROL_PERMISO').where('ID_Rol_Permiso', idRelacion).update({Estado: req.body.estado}).then( result => {
		if ( result ) {
			res.status(200).send({resp: 'Datos actualizados correctamente'});
		} else {
			res.status(500).send({ resp: 'error', error: 'No se pudo actualizar' });
		}
	}).catch(error => {
		res.status(500).send({resp: `${error}`});
	});
}

/*=====  End of actualizamos los permisosde los roles  ======*/


/*=====================================================================
=            Obtenemos los permisos a partir de un arreglo            =
=========================================================gi============*/

function GetPermisos(req, res) {
	let idRol = req.params.idRol;

	conn('ROL_PERMISO').where('ID_Rol', idRol).innerJoin('PERMISOS', 'ROL_PERMISO.ID_Permiso', 'PERMISOS.ID_Permiso').then(permisos => {
		if ( permisos ) {
			res.status(200).send({ resp: permisos });
		} else {
			res.status(404).send({resp: 'error', error: 'el rol no tiene permisos asigandos'});
		}
	}).catch(error => {
		res.status(500).send({resp:'error', error: `${error}`});
	});
}

/*=====  End of Obtenemos los permisos a partir de un arreglo  ======*/


/*==============================================================
=            Obtenemos los usuarios de un mismo rol            =
==============================================================*/

function GetUsuarios(req, res) {
	const idRol = req.params.idRol;

	conn('USER_ROL').where('ID_Rol', idRol).innerJoin('USUARIO', 'USER_ROL.ID_Usuario', 'USUARIO.ID_Usuario')
	.select('USUARIO.ID_Usuario', 'Nombre', 'Ap_Paterno', 'Ap_Materno', 'Foto', 'Color').then( usuarios => {
		if ( usuarios ) {
			res.status(200).send({ resp: usuarios });
		} else {
			res.status(404).send({resp: 'error', error: 'El rol no tiene usuarios asigandos'});
		}
	}).catch(error => {
		res.status(500).send({resp:'error', error: `${error}`});
	});
}

/*=====  End of Obtenemos los usuarios de un mismo rol  ======*/



module.exports = {
	ValidarRol,
	GetRoles,
	AddRol,
	UpdateRol,
	DeleteRol,
	GetRol,
	UpdatePermisos,
	GetPermisos,
	GetUsuarios
}