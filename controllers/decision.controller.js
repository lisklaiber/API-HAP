'use strict'

const CONN = require('./connection.js');
let Decision = require('../models/decision.controller.js');

/*==============================================
=            Agregamos una decision            =
==============================================*/

function AddDecision (req, res){
	let decision = new Decision(req.body.tipo, req.body.nombre, req.body.descripcion, 
		req.body.condicion);

	CONN('DECISIONES').insert(decision).then( idDecision => {
		if( idDecision )
			res.status(200).send({ resp: 'Decision agregada correctamente', idDecision: idDecision });
		else
			res.status(500).send({ resp: 'error', error: 'No se pudo agregar el elemento' });
	}).catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Agregamos una decision  ======*/


/*================================================
=            Actualizamos la decision            =
================================================*/

function UpdateDecision (req, res){
	let idDecision = req.params.idDecision;
	let decision = new Decision(req.body.tipo, req.body.nombre, req.body.descripcion, 
		req.body.condicion);

	CONN('DECISIONES').where('ID_Decision', idDecision).update(decision).then( result => {
		if( result )
			res.status(200).send({ resp: 'Datos actualizados correctamente' });
		else
			res.status(404).send({ resp: 'error', error: 'No se encontró el elemento a actualizar' });
	}).catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Actualizamos la decision  ======*/


/*===========================================================
=            Obtenemos los datos de una decision            =
===========================================================*/

function GetDecision (req, res){
	let idDecision = req.params.idDecision;

	CONN('DECISIONES').where('ID_Decision', idDecision).then( decision => {
		if( decision )
			res.status(200).send({ resp: decision[0] });
		else
			res.status(404).send({ resp:'error', error: 'No se encontró la decisión' });
	}).catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los datos de una decision  ======*/


/*===============================================
=            Eliminamos una decision            =
===============================================*/

function DeleteDecision (req, res){
	let idDecision = req.params.idDecision;

	CONN('DECISIONES').where('ID_Decision', idDecision).del().then( result => {
		res.status(200).send({ resp: 'Datos eliminados correctamente' });
	}).catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Eliminamos una decision  ======*/


/*==========================================================================
=            Obtenemos varias decisiones a partir de un arreglo            =
==========================================================================*/

function GetDecisiones(req, res){
	let idsDecisiones = req.body.idsDecisiones;

	CONN('DECISIONES').whereIn('ID_Decision', idsDecisiones).then( decisiones => {
		if( decisiones )
			res.status(200).send({ resp: decisiones });
		else
			res.status(404).send({ resp:'error', error: 'No se encontró ningún dato' });
	}).catch( error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos varias decisiones a partir de un arreglo  ======*/



module.exports = {
	AddDecision,
	UpdateDecision,
	GetDecision,
	DeleteDecision,
	GetDecisiones
}