'use strict'

const CONN = require('./connection.js');
let Level = require('../models/level.model.js');

/*==========================================
=            Agregamos un level            =
==========================================*/

function AddLevel (req, res) {
	let level = new Level(req.body.idProceso, req.body.padre, req.body.hijo, req.body.tipoHijo);

	CONN('LEVELS').insert(level).then( idLevel => {
		if( idLevel )
			res.status(200).send({ resp: idLevel });
		else
			res.status(500).send({ resp: 'error', error:'No se insertaron los datos' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Agregamos un level  ======*/


/*=============================================
=            Actualizamos un nivel            =
=============================================*/

function UpdateLevel (req, res) {
	let idLevel = req.params.idLevel;
	let level = new Level(req.body.idProceso, req.body.padre, req.body.hijo, req.body.tipoHijo);

	CONN('LEVELS').where('ID_Level', idLevel).update(level).then( result => {
		if( result )
			res.status(200).send({ resp: result });
		else
			res.status(500).send({ resp: 'error', error:'No se actualizaron los datos. No se encontró el level' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Actualizamos un nivel  ======*/


/*==========================================
=            Obtenemos un nivel            =
==========================================*/

function GetLevel (req, res){
	let idLevel = req.params.idLevel;

	CONN('LEVELS').where('ID_Level', idLevel).then( level => {
		if( level )
			res.status(200).send({ resp: level });
		else
			res.status(500).send({ resp: 'error', error:'No se encontró el level' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos un nivel  ======*/


/*=====================================================================
=            Obtenemos todos los hijos de un proceso padre            =
=====================================================================*/

function GetHijos (req, res){
	let idProcesoPadre = req.params.idProceso;

	CONN('LEVELS').where('Padre', idProcesoPadre).then( hijos => {
		if( hijos )
			res.status(200).send({ resp: hijos });
		else
			res.status(500).send({ resp: 0, message:'El proceso seleccionado no tiene hijos' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos todos los hijos de un proceso padre  ======*/


/*=====================================================================
=            Obtenemos todos los levels de un proceso base            =
=====================================================================*/

function GetLevels (req, res){
	let idProcesoPadre = req.params.idProceso;

	CONN('LEVELS').where('ID_Proceso', idProcesoPadre).then( levels => {
		if( levels )
			res.status(200).send({ resp: levels });
		else
			res.status(500).send({ resp: 0, message:'El proceso seleccionado no tiene hijos' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos todos los levels de un proceso base  ======*/


/*===========================================
=            Eliminamos un level            =
===========================================*/

function DeleteLevel (req, res){
	let idLevel = req.params.idLevel;

	CONN('LEVELS').where('ID_Level', idLevel).del().then( result => {
		res.status(200).send({ resp: 'level eliminado correctamene', result: result });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Eliminamos un level  ======*/


/*=======================================================================
=            Eliminamos todos los levels de un proceso padre            =
=======================================================================*/

function DeleteLevels (req, res){
	let idProcesoPadre = req.params.idProceso;

	CONN('LEVELS').where('Padre', idProcesoPadre).del().then( result => {
		res.status(200).send({ resp: 'levels eliminado correctamene', result: result });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Eliminamos todos los levels de un proceso padre  ======*/



module.exports = {
	AddLevel,
	UpdateLevel,
	GetLevel,
	GetHijos,
	GetLevels,
	DeleteLevel,
	DeleteLevels
}