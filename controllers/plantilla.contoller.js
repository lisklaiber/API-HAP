'use strict'
const conn = require('./connection.js');
let Plantilla = require('../models/plantilla.model.js');
let Proceso = require('../models/proceso.model.js');

/*=========================================================
=            Validación nombre de la plantilla            =
=========================================================*/

function ValidarPlantilla (req, res){
	let nombre = req.params.nombre;
	let idEmpresa = req.params.idEmpresa;
	conn('PLANTILLA').where({
		'Nombre': nombre,
		'ID_Empresa': idEmpresa
	}).select('Nombre').then(result => {
		if(result[0])
			res.send({
				resp: "El nombre seleccionado ya existe", 
				data: result[0]
			});
		else
			res.send({resp: "Aprovado", data: result[0]});
	}).catch(error => {
		res.status(500).send({resp: "Aprovado", Error: `${error}`});
	});
}

/*=====  End of Validación nombre de la plantilla  ======*/


/*============================================================
=            Agregar datos de una nueva plantilla            =
============================================================*/

function AddPlantilla(req, res){
	let plantilla = new Plantilla(req.body.nombre, req.body.descripcion, req.body.idEmpresa);
	conn('PLANTILLA').insert({
		Nombre: plantilla.nombre,
		Descripcion: plantilla.descripcion,
		ID_Empresa: plantilla.idEmpresa
	}).then(result => {
		res.status(200).send({
			resp: "Plantilla creada",
			data: result
		});
	}).catch(error => {
		res.status(500).send({
			res: "Error al crear la plantilla",
			Error: `${error}`
		});
	});
}

/*=====  End of Agregar datos de una nueva plantilla  ======*/


/*========================================================================
=            Funcion para obtener todas las platillas creadas            =
========================================================================*/

function GetPlantillas(req, res){
	let idEmpresa = req.params.idEmpresa;
	conn('PLANTILLA').where('ID_Empresa', idEmpresa).then(result => {
		if (!result) {
			res.status(404).send({resp: "No se han encontrado ningun template", templates: 0});
		}else{
			res.status(200).send({resp: result, templates: result.length});
		}
		
	}).catch(error => {
		res.status(500).send({resp:'error', error: `${error}`});
	});
}

/*=====  End of Funcion para obtener todas las platillas creadas  ======*/

/*=================================================
=            Actualizamos la plantilla            =
=================================================*/

function UpdatePlantilla(req, res){
	let idPlantilla = req.params.idPlantilla;
	let plantilla = new Plantilla(req.body.nombre, req.body.descripcion, req.body.idEmpresa);

	conn('PLANTILLA').where('ID_Plantilla').update(plantilla).then( result => {
		if( result )
			res.status(200).send({resp: 'Datos actualizados correctamente'});
		else
			res.status(500).send({resp: 'error', error: 'No se pudieron actualizar los datos'});
	} ).catch(error => {
		res.status(500).send({resp:'error', error: `${error}`});
	});
}

/*=====  End of Actualizamos la plantilla  ======*/



/*============================================================
=            Obtenemos los datos de una plantilla            =
============================================================*/

function GetPlantilla(req, res){
	let idPlantilla = req.params. idPlantilla;

	conn('PLANTILLA').where('ID_Plantilla', idPlantilla).then(plantilla =>{
		if(plantilla)
			res.staus(200).send({resp: plantilla});
		else {
			res.status(404).send({resp: 'no se encontró la palntilla seleccionada'});
		}
	}).catch(error => {
		res.stauts(500).send({ resp : 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los datos de una plantilla  ======*/


/*===================================================
=            elminación de una plantilla            =
===================================================*/

function DeletePlantilla(req, res){
	let idPlantilla = req.params.idPlantilla;

	conn('PLANTILLA').where('ID_Plantilla', idPlantilla).del().then(result => {
		if(result)
			res.status(200).send({ resp: result });
		else
			res.status(200).send({ resp: 'error al eliminar' });
	}).catch(error => {
		res.stauts(500).send({ resp : 'error', error: `${error}` });
	});
}

/*=====  End of elminación de una plantilla  ======*/



/*========================================================================
=            Funcion para insertar el proceso de la plantilla            =
========================================================================*/

function AddProcToPlant(req, res){
	let procesos = req.body.procesos;
	let levels = req.body.levels;
	let serials = req.body.serials;
	let insumos = req.body.insumos;

	conn('PROCESO').insert(procesos).then(result => {
		/*res.status(200).send({
			//resp: "Plantilla creada",
			resp: result
			//data: procesos
		});*/
		conn('PROCESO').where('ID_Plantilla', procesos[0].ID_Plantilla).select('ID_Proceso', 'Sub_ID').orderBy('Sub_ID', 'asc')
		.then(result => {
			for (let i = 0; i < levels.length; i++) {
				levels[i].Padre = result[levels[i].Padre - 1].ID_Proceso;
				levels[i].Hijo = result[levels[i].Hijo - 1].ID_Proceso;
			}
			for (let i = 0; i < serials.length; i++) {
				serials[i].ID_Sub_Proceso = result[serials[i].ID_Sub_Proceso - 1].ID_Proceso;
				serials[i].ID_Seriado = result[serials[i].ID_Seriado - 1].ID_Proceso;
			}
			insumos.map(insumo => {
				result.forEach(element => {
					if (insumo.ID_Proceso == element.Sub_ID)
						insumo.ID_Proceso = element.ID_Proceso
				});
			});

			/* Agregamos los insumos */
			insumos.forEach(insumo => {
				conn('INSUMO').insert({
					Nombre: insumo.Nombre,
					Obligatorio: insumo.Obligatorio,
					Tipo: insumo.Tipo,
					Descripcion: insumo.Descripcion
				}).then(idInsumo => {
					conn('PROC_INSUMO').insert({
						ID_Proceso: insumo.ID_Proceso,
						ID_Insumo: idInsumo[0]
					}).then( resultIns => {
						// res.status(200).send({
						// 	resp: "Se ha creado el proceso correctamente"
						// });
					}).catch(error => {
						res.status(500).send({ resp: "Error: " + error });
					});
				}).catch(error => {
					res.status(500).send({ resp: "Error: " + error });
				});
			});

			/* agregamos los niveles */
			conn('LEVELS').insert(levels).then(resultIns => {
				/*res.status(200).send({
					resp: resultIns
				});*/
			}).catch(err => {
				res.status(500).send({
					resp: {error: "" + err}
				});
				//agregar el eliminar los datos insertados cen aso de fallo
			});

			/* agregamos los seriales */
			conn('SERIALS').insert(serials).then(resultIns => {
				res.status(200).send({
					resp: "Se ha agregado el proceso correctamente"
					//Result: result, con esto se demuestra que se pueden devolver las promesas
					//ResultIns: resultIns
				});
			}).catch(err => {
				res.status(500).send({
					resp: {error: "" + err}
				});
				//agregar el eliminar los datos insertados cen aso de fallo
			});
			
		}).catch(err => {
			res.status(500).send({
				resp: {error: "" + err}
			});
			//agregar el eliminar los datos insertados cen aso de fallo
		});
	}).catch(err => {
		res.status(500).send({
			resp: {error: "" + err}
			//data: procesos
		});
	});
	
}

/*=====  End of Funcion para insertar el proceso de la plantilla  ======*/


/*=========================================================================
=            Funcion para obtener los procesos de la plantilla            =
=========================================================================*/

const GetProcesos = (idPlantilla) => {
	return conn('PROCESO').where('ID_Plantilla', idPlantilla);
}

const GetLevels = (idPlantilla) => {
	return conn('LEVELS').innerJoin('PROCESO', 'LEVELS.Padre', '=', 'PROCESO.ID_Proceso')
	.where('ID_Plantilla', idPlantilla).select('Padre', 'Hijo').orderBy('Padre', 'asc');
}

const GetSerials = (idPlantilla) => {
	return conn('SERIALS').innerJoin('PROCESO', 'SERIALS.ID_Sub_Proceso', '=', 'PROCESO.ID_Proceso')
	.where('ID_Plantilla', idPlantilla).select('ID_Sub_Proceso', 'ID_Seriado').orderBy('ID_Sub_Proceso', 'asc');
}

function GetProcPlant(req, res){
	let idPlantilla = req.params.idPlantilla;
	GetProcesos(idPlantilla).then(procesos => {
		let proceso = [];
		proceso.push(procesos);
		GetLevels(idPlantilla).then(levels => {
			proceso.push(levels);
			GetSerials(idPlantilla).then(serials =>{
				proceso.push(serials);
				res.status(200).send({
					procesos: proceso[0],
					levels: proceso[1],
					serials: proceso[2]
				});
			});
		});
	}).catch(err => {
		res.status(500).send({resp: {error: "" + err}});
	});
}

/*=====  End of Funcion para obtener los procesos de la plantilla  ======*/


/*==================================================================
=            Agregamos la relación de proceso plantilla            =
==================================================================*/

function AddProceso(req, res){
	let idPlantilla = req.body.idPlantilla;
	let idProceso = req.body.idProceso;

	conn('PROC_PLANT').insert({ ID_Plantilla: idPlantilla, ID_Proceso: idProceso }).then(idRelacion =>{
		if(idRelacion)
			res.status(200).send({ resp: 'datos ingresados correctamente', idRelacion: idRelacion })
		else
			res.status(404).send({ resp: 'error', error: 'no se pudieron insertar los datos' })
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Agregamos la relación de proceso plantilla  ======*/


/*=======================================================================
=            Eliminamos la relación de palntilla con proceso            =
=======================================================================*/

function DeleteProceso(req, res){
	let idRelacion = req.params.idRelacion;

	conn('PROC_PLANT').where('ID_Relacion', idRelacion).del().then(result =>{
		req.staus(200).send({ resp:'Relación eliminada' })
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});

}

/*=====  End of Eliminamos la relación de palntilla con proceso  ======*/


/*==================================================
=            Devolución de los métodos            =
==================================================*/

module.exports = {
	ValidarPlantilla,
	AddPlantilla,
	AddProcToPlant,
	GetPlantillas,
	GetProcPlant,
	GetProcesos,
	GetLevels,
	GetSerials,
	GetPlantilla,
	DeletePlantilla,
	AddProceso,
	DeleteProceso,
	UpdatePlantilla
}

/*=====  End of Devolución de los métodos  ======*/
