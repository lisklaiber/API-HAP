'use strict'

const CONN = require('./connection.js');
let Evento = require('../models/evento.model.js');

/*===========================================
=            Agregamos un evento            =
===========================================*/

function AddEvento(req, res){
	let evento = new Evento(req.body.nombre, req.body.descripcion);

	CONN('EVENTO').insert(evento).then(idEvento => {
		if( idEvento )
			res.status(200).send({ resp: 'Evento Agregado', idEvento: idEvento });
		else
			res.status(500).send({ resp: 'error', error: 'error al registar el evento' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Agregamos un evento  ======*/


/*==============================================
=            Actualizamos el evento            =
==============================================*/

function UpdateEvento(req, res){
	let idEvento = req.params.idEvento;
	let evento = new Evento(req.body.nombre, req.body.descripcion);

	CONN('EVENTO').where('ID_Evento', idEvento).update(evento).then( result => {
		if( result )
			res.status(200).send({ resp:'Datos actualizados correctamente', result: result });
		else
			res.status(500).send({ resp: 'error', error: 'No se pudo actualizar el evento' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Actualizamos el evento  ======*/

/*======================================================
=            Obtenemos los datos del evento            =
======================================================*/

function GetEvento(req, res){
	let idEvento = req.params.idEvento;

	CONN('EVENTO').where('ID_Evento', idEvento).then( evento => {
		if( evento )
			res.status(200).send({ resp: evento[0] });
		else
			res.status(404).send({ resp: 'error', error: 'No se encontró el evento' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos los datos del evento  ======*/


/*============================================
=            Eliminamos el evento            =
============================================*/

function DeleteEvento(req, res){
	let idEvento = req.params.idEvento;

	CONN('EVENTO').where('ID_Evento', idEvento).del().then( rsult => {
		res.status(200).send({ resp: 'Evento eliminado correctamente' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Eliminamos el evento  ======*/


/*=======================================================================
=            Obtenemos varios eventos a partir de un arreglo            =
=======================================================================*/

function GetEventos (req, res) {
	let idsEventos = req.body.idsEventos;

	CONN('EVENTO').whereIn('ID_Evento', idsEventos).then( eventos => {
		if( eventos )
			res.status(200).send({ resp: eventos });
		else
			res.status(404).send({ resp: 'error', error: 'Eventos no encontrados' });
	}).catch( error => {
		res.status(500).send({ resp: "error", error: `${error}` });
	});
}

/*=====  End of Obtenemos varios eventos a partir de un arreglo  ======*/



module.exports = {
	AddEvento,
	UpdateEvento,
	GetEvento,
	DeleteEvento,
	GetEventos
}