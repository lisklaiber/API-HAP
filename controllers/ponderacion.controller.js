'use strict'

const CONN = require('./connection.js');
let Ponderacion = require('../models/ponderacion.model.js');


/*=================================================
=            Agregamos una ponderación            =
=================================================*/

function AddPonderacion(req, res) {
	let ponderacion = new Ponderacion(req.body.completado, req.body.tiempo, req.body.costo, req.body.idProceso);

	CONN('PONDERACION_INDICADORES').insert(ponderacion).then( idPonderacion => {
		if( idPonderacion )
			res.status(200).send({ resp: 'Ponderacion agregada', id: idPonderacion });
		else
			res.status(500).send({ resp: 'error', error: 'No se pudo agregar la ponderación' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Agregamos una ponderación  ======*/


/*====================================================
=            Actualizamos una ponderación            =
====================================================*/

function UpdatePonderacion(req, res) {
	let idPonderacion = req.params.idPonderacion;
	let ponderacion = new Ponderacion(req.body.completado, req.body.tiempo, req.body.costo, req.body.idProceso);

	CONN('PONDERACION_INDICADORES').where('ID_Ponderacion', idPonderacion).update(ponderacion).then( result => {
		if( result )
			res.status(200).send({ resp: 'Ponderacion actualizada', result: result });
		else
			res.status(500).send({ resp: 'error', error: 'No se pudo actualizar la ponderación' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Actualizamos una ponderación  ======*/


/*==============================================================
=            Obtenemos los datos de una ponderación            =
==============================================================*/

function GetPonderacion(req, res){
	let idPonderacion = req.params.idPonderacion;

	CONN('PONDERACION_INDICADORES').where('ID_Ponderacion', idPonderacion).then( ponderacion => {
		if( ponderacion )
			res.status(200).send({ resp: ponderacion });
		else
			res.status(500).send({ resp: 'error', error: 'No se encontró la ponderación' });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Obtenemos los datos de una ponderación  ======*/

/*==================================================
=            Eliminamos una ponderacion            =
==================================================*/

function DeletePonderacion (req, res){
	let idPonderacion = req.params.idPonderacion;

	CONN('PONDERACION_INDICADORES').where('ID_Ponderacion', idPonderacion).del().then( result => {
		res.status(200).send({ resp: 'Se eliminó la poderación', result: result });
	}).catch(error => {
		res.status(500).send({ resp: 'error', error: `${error}` });
	});
}

/*=====  End of Eliminamos una ponderacion  ======*/



module.exports = {
	AddPonderacion,
	UpdatePonderacion,
	GetPonderacion,
	DeletePonderacion
}